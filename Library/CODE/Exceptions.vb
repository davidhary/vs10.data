''' <summary>
''' Reports an exception caused because an SQL server was not found.
''' </summary>
''' <license>
''' (c) 2006 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="11/08/06" by="David Hary" revision="1.0.2503.x">
''' created.
''' </history>
<Serializable()> Public Class LocalServerNotFoundException

    Inherits isr.Data.BaseException

    Private Const _defMessage As String = "No local SQL Server is installed."

    ''' <summary>
    ''' <summary>Constructor with no parameters.</summary>
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
        Me.New(_defMessage)
    End Sub

    ''' <summary>
    ''' Constructs the class specifying a <paramref name="message">message</paramref>.
    ''' </summary>
    ''' <param name="message">
    ''' Specifies the exception message.
    ''' </param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>Constructor specifying the Message and InnerException property to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Sets a reference to the InnerException.</param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary>
    ''' Constructs the class specifying a <paramref name="message">message</paramref>
    ''' and <paramref name="innerException"/>.
    ''' </summary>
    ''' <param name="innerException">Sets a reference to the InnerException.</param>
    ''' <param name="format">Specifies the exception message format.</param>
    ''' <param name="args">Specifies the exception message arguments.</param>
    Public Sub New(ByVal innerException As System.Exception, ByVal format As String, ByVal ParamArray args() As Object)
        MyBase.New(innerException, format, args)
    End Sub

    ''' <summary>
    ''' Constructs the class using serialization <paramref name="info"/> and <paramref name="context"/>
    '''  information.
    ''' </summary>
    ''' <param name="info">
    ''' Specifies <see cref="System.Runtime.Serialization.SerializationInfo">serialization information</see>.
    ''' </param>
    ''' <param name="context">
    ''' Sepecifies <see cref="System.Runtime.Serialization.StreamingContext">streaming context</see> for the exception.
    ''' </param>
    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, _
        ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

End Class

