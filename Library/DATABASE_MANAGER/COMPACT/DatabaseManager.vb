﻿Namespace Compact

    ''' <summary>
    ''' Implements a database manager entity for SQL Compact Edition.</summary>
    ''' <license>
    ''' (c) 2009 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="10/21/2009" by="David Hary" revision="1.2.3581.x">
    ''' created.
    ''' </history>
    ''' <history date="09/16/2011" by="David Hary" revision="1.2.4276.x">
    ''' Use namespace isr.Data.Compact.
    ''' </history>
    Public Class DatabaseManager

        Inherits DatabaseManagerBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>
        ''' Construtor for this class.
        ''' </summary>
        ''' <param name="connectionString">Specifies the connection string.</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal connectionString As String)
            MyBase.New(connectionString, ".sdf")
            ' MyBase.DefaultConnectionInfo = New ConnectionInfo(ConnectionInfo.BuildConnectionString(MyBase.DatabaseName, "Database", MyBase.DatabaseFileInfo.Extension))
            CurrentReleaseVersionQueryTextSetter("SELECT ReleaseRevision, IsCurrentRelease FROM ReleaseHistory WHERE (IsCurrentRelease = 1)")
        End Sub

#End Region

#Region " I CONNECTION INFO "

        ''' <summary>
        ''' Locates teh database file.
        ''' </summary>
        ''' <param name="timeout"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        ''' <example>
        ''' <code>
        ''' </code>
        ''' </example>
        Public Overrides Function PingHost(ByVal timeout As Integer) As Boolean
            Return Me.LocateDatabase()
        End Function

        ''' <summary>
        ''' Returns true if able to connect to the database using the specified connection string.
        ''' </summary>
        ''' <param name="connectionString">Specifies the connection string to connect to.</param>
        ''' <param name="keepConnectionOpen">True to keep connection open.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overrides Function TryConnect(ByVal connectionString As String, ByVal keepConnectionOpen As Boolean) As Boolean

            If String.IsNullOrEmpty(connectionString) Then
                Throw New ArgumentNullException("connectionString")
            End If

            Dim connection As System.Data.SqlServerCe.SqlCeConnection = Nothing
            Try

                ' connect to the local database.
                connection = New System.Data.SqlServerCe.SqlCeConnection(connectionString)
                connection.Open()
                MyBase.LastConnectionFailed = connection.State <> ConnectionState.Open
                Return Not MyBase.LastConnectionFailed

            Catch ex As System.Data.SqlServerCe.SqlCeException

                MyBase.EnqueueMessage("Exception occurred attempting to connect using connection string '{0}'. Details: {1}{2}.", _
                                      connectionString, Environment.NewLine, ex)

            Finally
                If connection IsNot Nothing Then
                    If Not keepConnectionOpen Then
                        If connection.State = ConnectionState.Open Then
                            connection.Close()
                        End If
                        connection.Dispose()
                    End If
                End If
            End Try
            Return False

        End Function

#End Region

#Region " COMMAND "

        ''' <summary>
        ''' Executes the specified command and returns the number of rows affected.
        ''' </summary>
        ''' <param name="connectionString">Specifies the connection string.</param>
        ''' <param name="commandText">Specifies a command</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overrides Function ExecuteCommand(ByVal connectionString As String, ByVal commandText As String) As Integer

            Dim rowCount As Integer = 0
            Using connection As System.Data.SqlServerCe.SqlCeConnection = New System.Data.SqlServerCe.SqlCeConnection(connectionString)
                Dim sqlCommand As System.Data.SqlServerCe.SqlCeCommand = connection.CreateCommand()
                sqlCommand.CommandText = commandText
                connection.Open()
                rowCount = sqlCommand.ExecuteNonQuery()
                connection.Close()
            End Using
            Return rowCount

        End Function

#End Region

#Region " DATABASE "

        ''' <summary>
        ''' Backs up and archives the database. With CE we must archive (givbe the database a new date file name 
        ''' because there are not incremental in-file backups like in regular SQL.
        ''' </summary>
        ''' <remarks></remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
        Public Overrides Function BackupDatabase() As Boolean

            Dim lastAction As String = "Backing up database."

            Try
                Dim backupFileName As String = MyBase.DatabaseName & _
                                               String.Format(Globalization.CultureInfo.CurrentCulture, _
                                                  MyBase.TimestampFormat & MyBase.DefaultBackupFileExtension, DateTime.Now)
                backupFileName = System.IO.Path.Combine(MyBase.BackupFileInfo.Directory.FullName, backupFileName)
                MyBase.BackupFileInfo = New System.IO.FileInfo(backupFileName)
                lastAction = "Backing up database to '" & MyBase.BackupFileInfo.FullName & "'"
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
                System.IO.File.Copy(Me.DatabaseFileInfo.FullName, MyBase.BackupFileInfo.FullName)
                Return True

            Catch ex As System.Exception

                My.Application.Log.WriteException(ex, TraceEventType.Error, lastAction)
                Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "{0} failed {1}: {2}.", _
                    Me, lastAction, ex)
                MyBase.EnqueueMessage(message)
                Return False

            End Try

        End Function

        ''' <summary>
        ''' Creates a new database
        ''' </summary>
        ''' <param name="connectionString">Specifies the database connection string</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
        Public Overrides Function CreateDatabase(ByVal connectionString As String) As Boolean

            Dim engine As New System.Data.SqlServerCe.SqlCeEngine
            Dim lastAction As String = String.Empty
            Try

                lastAction = "Creating database."
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
                engine.LocalConnectionString = connectionString
                engine.CreateDatabase()
                Return True

            Catch ex As System.Exception

                My.Application.Log.WriteException(ex, TraceEventType.Error, lastAction)
                Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "{0} failed {1}: {2}.", _
                    Me, lastAction, ex)
                MyBase.EnqueueMessage(message)

            Finally

                If engine IsNot Nothing Then
                    engine.Dispose()
                    engine = Nothing
                End If

            End Try

        End Function

        ''' <summary>
        ''' Drops (deletes) the current database.
        ''' </summary>
        ''' <remarks></remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
        Public Overrides Function DropDatabase() As Boolean

            Dim lastAction As String = String.Empty

            Try

                lastAction = "Deleting database file '" & Me.DatabaseFileInfo.FullName & "'"
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
                System.IO.File.Delete(Me.DatabaseFileInfo.FullName)
                MyBase.IsDatabaseExistsSetter(False)
                Return True

            Catch ex As Exception

                Return False

            Finally

            End Try

        End Function

        ''' <summary>
        ''' Looks for the the database and returns true if exists.  Sets the local cache 
        ''' <see cref="IsDatabaseExists"/> for the condition.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overrides Function LocateDatabase() As Boolean

            Me.DatabaseFileInfo.Refresh()
            MyBase.IsDatabaseExistsSetter(Me.DatabaseFileInfo.Exists)
            Return MyBase.IsDatabaseExists

        End Function

#End Region

#Region " QUERY "

#If False Then
  ''' <summary>
  ''' Reads a string type field from the database using the specified query..
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function QueryString(ByVal connectionString As String, ByVal queryText As String) As String
    Using connection As New System.Data.SqlServerCe.SqlCeConnection(connectionString)
      Dim command As New System.Data.SqlServerCe.SqlCeCommand()
      command.Connection = connection
      command.CommandType = System.Data.CommandType.Text
      command.CommandText = queryText
      command.Connection.Open()
      Dim reader As System.Data.SqlServerCe.SqlCeDataReader = command.ExecuteResultSet( _
                                                                System.Data.SqlServerCe.ResultSetOptions.Scrollable)
      Try
        If reader.HasRows Then
          reader.Read()
          Return reader.GetString(0)
        Else
          Return FailedRelease
        End If
      Finally
        reader.Close()
        command.Connection.Close()
      End Try

    End Using

  End Function
#End If

#End Region

#Region " READER "

        ''' <summary>
        ''' Executes the specified query command and returns reference to the 
        ''' <see cref="ConnectedReader">connected reader</see>
        ''' </summary>
        ''' <param name="connectionString">Specifies the connection string.</param>
        ''' <param name="queryCommand">Specifies a query</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overrides Function ExecuteReader(ByVal connectionString As String, ByVal queryCommand As String) As ConnectedReader

            Return ExecuteReader(New System.Data.SqlServerCe.SqlCeConnection(connectionString), queryCommand)

        End Function

        ''' <summary>
        ''' Executes the specified query command and returns reference to the 
        ''' <see cref="ConnectedReader">connected reader</see>
        ''' </summary>
        ''' <param name="connection">Specifies a <see cref="System.Data.SqlServerCe.SqlCeConnection">connection</see>.</param>
        ''' <param name="queryCommand">Specifies a query</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overloads Shared Function ExecuteReader(ByVal connection As System.Data.SqlServerCe.SqlCeConnection, ByVal queryCommand As String) As ConnectedReader

            Dim command As New System.Data.SqlServerCe.SqlCeCommand()
            command.Connection = connection
            command.CommandType = System.Data.CommandType.Text
            command.CommandText = queryCommand
            If connection.State <> ConnectionState.Open Then
                command.Connection.Open()
            End If
            Dim reader As System.Data.SqlServerCe.SqlCeDataReader = command.ExecuteResultSet( _
                                                                      System.Data.SqlServerCe.ResultSetOptions.Scrollable)
            Return New ConnectedReader(connection, reader)

        End Function

#End Region

#Region " TABLES "

#If False Then
  ''' <summary>
  ''' Returns true if a record exists sutifying the query command.
  ''' </summary>
  ''' <param name="connectionString">Specifies the connection string.</param>
  ''' <param name="queryCommand">Specifies a query</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function RecordExists(ByVal connectionString As String, ByVal queryCommand As String) As Boolean

    Dim connReader As ConnectedReader = Me.ExecuteReader(connectionString, queryCommand)
    RecordExists = connReader.Reader.Read
    connReader.CloseConnection()

    Using connection As New System.Data.SqlServerCe.SqlCeConnection(connectionString)
      Dim command As New System.Data.SqlServerCe.SqlCeCommand()
      command.Connection = connection
      command.CommandType = System.Data.CommandType.Text
      command.CommandText = queryCommand
      command.Connection.Open()
      Dim reader As System.Data.SqlServerCe.SqlCeDataReader = command.ExecuteResultSet( _
                                                                System.Data.SqlServerCe.ResultSetOptions.Scrollable)
      RecordExists = reader.HasRows
      reader.Close()
      command.Connection.Close()

    End Using
  End Function
#End If

#End Region

#Region " UPDATE QUERY "

        ''' <summary>
        '''  Executes a table update query.
        ''' </summary>
        ''' <returns>The total number of minor errors minus the number of successful command executions</returns>
        ''' <remarks></remarks>
        ''' <param name="query">Specifies the query of file name where the query is located.
        ''' A file name begins with '@'</param>
        ''' <param name="connectionString">Specifies the database connection string</param>
        ''' <param name="ignoreMinorErrors">Allows ignoring minor SQL error on a complex multi-line command.</param>
        Public Overrides Function UpdateTableQuery(ByVal connectionString As String, ByVal query As String, ByVal ignoreMinorErrors As Boolean) As Integer
            Return UpdateTableQuery(connectionString, DatabaseManagerBase.PrepareCommandQuery(query), ignoreMinorErrors)
        End Function

        ''' <summary>
        '''  Executes a table update query.
        ''' </summary>
        ''' <returns>The total number of minor errors minus the number of successful command executions</returns>
        ''' <remarks></remarks>
        ''' <param name="query">Specifies the query of file name where the query is located.
        ''' A file name begins with '@'</param>
        ''' <param name="connectionString">Specifies the database connection string</param>
        ''' <param name="ignoreMinorErrors">Allows ignoring minor SQL error on a complex multi-line command.</param>
        Public Overrides Function UpdateTableQuery(ByVal connectionString As String, ByVal query As String(), ByVal ignoreMinorErrors As Boolean) As Integer

            Using connection As New System.Data.SqlServerCe.SqlCeConnection(connectionString)
                Return UpdateTableQuery(connection, query, ignoreMinorErrors)
            End Using

        End Function

        ''' <summary>
        '''  Executes a table update query.
        ''' </summary>
        ''' <returns>The total number of minor errors minus the number of successful command executions</returns>
        ''' <remarks></remarks>
        ''' <param name="query">Specifies the query of file name where the query is located.
        ''' A file name begins with '@'</param>
        ''' <param name="connection ">Specifies the database connection</param>
        ''' <param name="ignoreMinorErrors">Allows ignoring minor SQL error on a complex multi-line command.</param>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
        Public Overloads Function UpdateTableQuery(ByVal connection As System.Data.SqlServerCe.SqlCeConnection, ByVal query As String(), ByVal ignoreMinorErrors As Boolean) As Integer

            Dim recordCount As Int32
            Dim isOpened As Boolean
            MyBase.QueryUpdateCountSetter(0)
            MyBase.QueryErrorCountSetter(0)
            If query IsNot Nothing Then
                Dim command As New System.Data.SqlServerCe.SqlCeCommand()
                command.Connection = connection
                command.CommandType = System.Data.CommandType.Text
                If connection.State <> ConnectionState.Open Then
                    isOpened = True
                    connection.Open()
                End If
                For Each queryCommand As String In query
                    If Not String.IsNullOrEmpty(queryCommand) Then
                        ' trim any leading or lagging control characters and spaces.
                        queryCommand = queryCommand.Trim
                        If Not String.IsNullOrEmpty(queryCommand) Then
                            command.CommandText = queryCommand
                            If ignoreMinorErrors Then
                                Try
                                    ' todo: Check on this.
                                    ' each update returns -1 if success or and error  if failed.
                                    recordCount += command.ExecuteNonQuery()
                                    MyBase.QueryUpdateCountSetter(MyBase.QueryUpdateCount + 1)
                                Catch ex As Exception
                                    MyBase.QueryErrorCountSetter(MyBase.QueryErrorCount + 1)
                                Finally
                                End Try
                            Else
                                recordCount += command.ExecuteNonQuery()
                                MyBase.QueryUpdateCountSetter(MyBase.QueryUpdateCount + 1)
                            End If
                        End If
                    End If
                Next
                If isOpened Then
                    connection.Close()
                End If
            End If
            Return recordCount

        End Function

#End Region

    End Class

End Namespace
