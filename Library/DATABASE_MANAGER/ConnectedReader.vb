﻿''' <summary>
''' Implements a database connection with an open reader.</summary>
''' <license>
''' (c) 2009 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/21/2009" by="David Hary" revision="1.2.3581.x">
''' created.
''' </history>
Public Class ConnectedReader

    Implements IConnectedReader, IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Construtor for this class.
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    Public Sub New(ByVal connection As IDbConnection, ByVal reader As IDataReader)
        MyBase.New()
        _connection = connection
        _reader = reader
    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    Private _isDisposed As Boolean
    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return _isDisposed
        End Get
        Private Set(ByVal value As Boolean)
            _isDisposed = value
        End Set
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    If _reader IsNot Nothing Then
                        _reader.Close()
                        _reader.Dispose()
                    End If

                    If _connection IsNot Nothing Then
                        _connection.Close()
                        _connection.Dispose()
                    End If

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            Me.IsDisposed = True

        End Try

    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal in terms of
        ' readability and maintainability.
        Dispose(False)
        ' The compiler automatically adds a call to the base class finalizer 
        ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
        MyBase.Finalize()

    End Sub

#End Region

    Private _connection As IDbConnection
    ''' <summary>
    ''' Gets the connection.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Connection() As System.Data.IDbConnection Implements IConnectedReader.Connection
        Get
            Return _connection
        End Get
    End Property

    Private _reader As System.Data.IDataReader
    ''' <summary>
    ''' Gets the reader.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Reader() As System.Data.IDataReader Implements IConnectedReader.Reader
        Get
            Return _reader
        End Get
    End Property

    ''' <summary>
    ''' Closes the read and the connection.
    ''' </summary>
    ''' <remarks></remarks>
    Sub CloseConnection() Implements IConnectedReader.CloseConnection

        If _reader IsNot Nothing Then
            _reader.Close()
            _reader.Dispose()
        End If

        If _connection IsNot Nothing Then
            _connection.Close()
            _connection.Dispose()
        End If

    End Sub

End Class
