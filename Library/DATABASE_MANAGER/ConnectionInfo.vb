﻿''' <summary>
''' Parses connection strings.</summary>
''' <license>
''' (c) 2009Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/23/09" by="David Hary" revision="1.2.3573.x">
''' created.
''' </history>
Public Class ConnectionInfo

    Implements isr.Core.IConnectionInfo

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Construtor for this class.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New(ByVal connectionString As String)
        MyBase.New()
        Me.ConnectionString = connectionString
    End Sub

    ''' <summary>
    ''' Construtor for this class.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New(ByVal connectionString As String, ByVal databaseName As String)
        Me.New(connectionString)
        _databaseName = databaseName
    End Sub

#End Region

#Region " I CONNECTION INFO "

    Public Event ConnectionStringChanged(ByVal sender As Object, ByVal e As System.EventArgs) Implements Core.IConnectionInfo.ConnectionStringChanged

    Private _dataSourceCompatibilityLevel As Integer
    ''' <summary>
    ''' Gets or sets the SQL compatibility level for this entity. 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property DataSourceCompatibilityLevel() As Integer Implements Core.IConnectionInfo.DataSourceCompatibilityLevel
        Get
            Return _dataSourceCompatibilityLevel
        End Get
        Set(ByVal value As Integer)
            _dataSourceCompatibilityLevel = value
        End Set
    End Property

    Private _connectionString As String
    ''' <summary>
    ''' Gets the connection string.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ConnectionString() As String Implements isr.Core.IConnectionInfo.ConnectionString
        Get
            Return _connectionString
        End Get
        Set(ByVal value As String)
            Dim isChanged As Boolean = Not (String.IsNullOrEmpty(value) OrElse String.IsNullOrEmpty(_connectionString) OrElse value = _connectionString)
            If Not String.IsNullOrEmpty(value) Then
                _parseConnectionString(value)
            End If
            _connectionString = value
            If isChanged Then
                If ConnectionStringChangedEvent IsNot Nothing Then ConnectionStringChangedEvent(Me, System.EventArgs.Empty)
            End If
        End Set
    End Property

    Private _databaseFileInfo As System.IO.FileInfo
    ''' <summary>
    ''' Gets the database file information.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property DatabaseFileInfo() As System.IO.FileInfo Implements isr.Core.IConnectionInfo.DatabaseFileInfo
        Get
            Return _databaseFileInfo
        End Get
    End Property

    Private _databaseName As String
    ''' <summary>
    ''' Gets the database name.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property DatabaseName() As String Implements isr.Core.IConnectionInfo.DatabaseName
        Get
            Return _databaseName
        End Get
    End Property

    Private _instanceName As String '
    ''' <summary>
    ''' Gets the database server instance name.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property InstanceName() As String Implements isr.Core.IConnectionInfo.InstanceName
        Get
            Return _instanceName
        End Get
    End Property

    Private _serverName As String
    ''' <summary>
    ''' Gets the database server name.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ServerName() As String Implements isr.Core.IConnectionInfo.ServerName
        Get
            Return _serverName
        End Get
    End Property

#End Region

#Region " BUILDERS "

    ''' <summary>
    ''' Builds a default CE connection string.
    ''' </summary>
    ''' <param name="databaseName"></param>
    ''' <remarks></remarks>
    Public Shared Function BuildConnectionString(ByVal databaseName As String, ByVal databaseFolderTitle As String, ByVal databaseFileExtension As String) As String
        Dim folderPathName As String = isr.Core.SpecialFolders.BuildApplicationDataFolderName(True, True, databaseFolderTitle, True)
        Dim filePathName As String = System.IO.Path.Combine(folderPathName, databaseName & databaseFileExtension)
        Return String.Format(Globalization.CultureInfo.InvariantCulture, "Data Source={0}", filePathName)
    End Function

    Private Shared _connectionStringPattern As String = "data source={0};initial catalog={1};integrated security=SSPI;persist security info=False"
    ''' <summary>
    ''' Gets or set the default connection string format pattern.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Property ConnectionStringPattern() As String
        Get
            Return _connectionStringPattern
        End Get
        Set(ByVal value As String)
            _connectionStringPattern = value
        End Set
    End Property

    ''' <summary>
    ''' Returns a connection string to an SQL catalog on the specified local server instance.
    ''' </summary>
    ''' <param name="pattern">Specifies the connection string pattern.</param>
    ''' <param name="instanceName">Specifies the local server instance name.</param>
    ''' <param name="databaseName">Specifies the database name.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function BuildLocalConnectionString(ByVal pattern As String, ByVal instanceName As String, ByVal databaseName As String) As String

        If String.IsNullOrEmpty(pattern) Then
            Throw New ArgumentNullException("pattern")
        End If

        If String.IsNullOrEmpty(databaseName) Then
            Throw New ArgumentNullException("databaseName")
        End If

        If instanceName Is Nothing Then
            instanceName = String.Empty
        End If

        Return String.Format(Globalization.CultureInfo.CurrentCulture, _
              pattern, ConnectionInfo.BuildLocalServerInstanceName(instanceName), databaseName)

    End Function

    ''' <summary>
    ''' Returns a server name in the format 'Machine Name' or'Machine Name\Instance name', e.g.,
    ''' "ABC\SQLEXPRESS".
    ''' </summary>
    ''' <param name="instanceName">Specifies the local server instance name.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function BuildLocalServerInstanceName(ByVal instanceName As String) As String

        If instanceName Is Nothing Then
            instanceName = String.Empty
        End If

        Dim dataSourcePattern As String = "{0}\{1}"

        If String.IsNullOrEmpty(instanceName) Then
            Return "localhost" '  Environment.MachineName
        Else
            Return String.Format(Globalization.CultureInfo.CurrentCulture, _
                    dataSourcePattern, Environment.MachineName, instanceName)
        End If

    End Function

    ''' <summary>
    ''' Returns a connection string to an SQL catalog on the specified local server instance.
    ''' </summary>
    ''' <param name="instanceName">Specifies the local server instance name.</param>
    ''' <param name="databaseName">Specifies the database name.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function BuildLocalConnectionString(ByVal instanceName As String, ByVal databaseName As String) As String

        If instanceName Is Nothing Then
            instanceName = String.Empty
        End If

        If String.IsNullOrEmpty(databaseName) Then
            Throw New ArgumentNullException("databaseName")
        End If

        Return ConnectionInfo.BuildLocalConnectionString(_connectionStringPattern, instanceName, databaseName)

    End Function

#End Region

#Region " PARSERS "

    Private Sub _parseCompactConnectionString(ByVal connectionString As String)

        _serverName = "SQLCE"
        _instanceName = "SQLCE"
        _databaseFileInfo = New System.IO.FileInfo(connectionString.Split("="c)(1).Trim.Trim(";"c))
        _databaseName = isr.Core.IOExtensions.Title(_databaseFileInfo)
        _dataSourceCompatibilityLevel = 4

    End Sub

    ''' <summary>
    ''' Parses the connection string getting the server, instance,and database names.
    ''' </summary>
    ''' <param name="connectionString"></param>
    ''' <remarks></remarks>
    Private Sub _parseConnectionString(ByVal connectionString As String)

        If String.IsNullOrEmpty(connectionString) Then
            Throw New ArgumentNullException("connectionString")
        End If

        If connectionString.ToUpperInvariant.Contains(".SDF") Then
            _parseCompactConnectionString(connectionString)
        Else
            _parseFullConnectionString(connectionString)
        End If

    End Sub

    ''' <summary>
    ''' Parses the connection string getting the server, instance,and database names.
    ''' </summary>
    ''' <param name="connectionString"></param>
    ''' <remarks></remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
    Private Sub _parseFullConnectionString(ByVal connectionString As String)

        ' data source=MUSCAT\SQLEXPRESS;initial catalog=Provers70;persist security info=False;User id=bank;Password=syncWithMe;
        _dataSourceCompatibilityLevel = 2
        Dim syntaxParts As String() = connectionString.Split(";"c)
        Dim dataSources As String() = syntaxParts(0).Split("="c)
        Dim serverElements As String() = dataSources(1).Split("\"c)
        If serverElements IsNot Nothing Then
            If serverElements.Length > 0 Then
                _serverName = serverElements(0)
            End If
            If serverElements.Length > 1 Then
                _instanceName = serverElements(1)
            End If
        End If
        Dim databaselements As String() = syntaxParts(1).Split("="c)
        If databaselements IsNot Nothing Then
            If databaselements.Length > 1 Then
                _databaseName = databaselements(1)
            End If
        End If

        _databaseFileInfo = New System.IO.FileInfo("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Data\Prover70.sql")
        If Not (String.IsNullOrEmpty(_serverName) Or String.IsNullOrEmpty(_instanceName)) Then
            ' try getting a file info.
            Try
                Dim server As Microsoft.SqlServer.Management.Smo.Server
                server = Sql.DatabaseManager.ConnectLocalServer(_instanceName)
                Dim folderPathName As String = server.Information.MasterDBPath
                Dim filePathName As String = System.IO.Path.Combine(folderPathName, _databaseName & ".sql")
                _databaseFileInfo = New System.IO.FileInfo(filePathName)
            Catch
            Finally
            End Try
        End If

    End Sub

#End Region

End Class
