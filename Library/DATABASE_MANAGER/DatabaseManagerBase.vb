﻿''' <summary>
''' Base class for implemenitng a personnel entity for the Provers database.</summary>
''' <license>
''' (c) 2009 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/21/2009" by="David Hary" revision="1.2.3581.x">
''' created.
''' </history>
Public MustInherit Class DatabaseManagerBase

    Implements IDisposable, IDatabaseManager

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Construtor for this class.
    ''' </summary>
    ''' <param name="connectionString">Specifies the connection string.</param>
    ''' <remarks></remarks>
    Protected Sub New(ByVal connectionString As String)
        Me.New(connectionString, ".bak")
    End Sub

    ''' <summary>
    ''' Construtor for this class.
    ''' </summary>
    ''' <param name="connectionString">Specifies the connection string.</param>
    ''' <param name="backupFileExtension">Specifies the backup file extension</param>
    ''' <remarks></remarks>
    Protected Sub New(ByVal connectionString As String, ByVal backupFileExtension As String)
        MyBase.New()
        _defaultBackupFileExtension = backupFileExtension
        _defaultBackupFolderTitle = "Backup"
        _messageQueue = New isr.Core.MessageQueue
        _currentReleaseVersion = ""
        _timestampFormat = "-{0:yyyyMMdd}{0:HHmmssff}"
        Me.ConnectionInfoSetter(New ConnectionInfo(connectionString))
        ' _defaultconnectionInfo = New ConnectionInfo(connectionString)
    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    Private _isDisposed As Boolean
    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return _isDisposed
        End Get
        Private Set(ByVal value As Boolean)
            _isDisposed = value
        End Set
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    If _messageQueue IsNot Nothing Then
                        _messageQueue.Clear()
                        _messageQueue = Nothing
                    End If

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            Me.IsDisposed = True

        End Try

    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal in terms of
        ' readability and maintainability.
        Dispose(False)
        ' The compiler automatically adds a call to the base class finalizer 
        ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
        MyBase.Finalize()

    End Sub

#End Region

#Region " I CONNECTION INFO "

    Public Event ConnectionStringChanged(ByVal sender As Object, ByVal e As System.EventArgs) Implements isr.Core.IConnectionInfo.ConnectionStringChanged

    Private Sub _connectionInfo_ConnectionStringChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _connectionInfo.ConnectionStringChanged
        If ConnectionStringChangedEvent IsNot Nothing Then ConnectionStringChangedEvent(Me, e)
    End Sub

    ''' <summary>
    ''' Gets or sets reference to the <see cref="ConnectionInfo">Connection Info entity.</see>
    ''' </summary>
    ''' <remarks></remarks>
    Private WithEvents _connectionInfo As ConnectionInfo

    ''' <summary>
    ''' Sets the connection string.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <remarks></remarks>
    Private Sub ConnectionInfoSetter(ByVal value As ConnectionInfo)
        _connectionInfo = value
        If _backupFileInfo Is Nothing Then
            backupFileInfoSetter(value)
        End If
    End Sub

    ''' <summary>
    ''' Gets or sets the connection string.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overridable Property ConnectionString() As String Implements isr.Core.IConnectionInfo.ConnectionString
        Get
            Return _connectionInfo.ConnectionString
        End Get
        Set(ByVal value As String)
            _connectionInfo.ConnectionString = value
            Me.ConnectionInfoSetter(_connectionInfo)
        End Set
    End Property

    ''' <summary>
    ''' Gets the database name.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property DatabaseName() As String Implements isr.Core.IConnectionInfo.DatabaseName
        Get
            Return _connectionInfo.DatabaseName
        End Get
    End Property

    ''' <summary> 
    ''' Gets or sets the SQL compatibility level for this entity. 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property DataSourceCompatibilityLevel() As Integer Implements isr.Core.IConnectionInfo.DataSourceCompatibilityLevel
        Get
            Return _connectionInfo.DataSourceCompatibilityLevel
        End Get
        Set(ByVal value As Integer)
            _connectionInfo.DataSourceCompatibilityLevel = value
        End Set
    End Property

    ''' <summary>
    ''' Gets the  database file information.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property DatabaseFileInfo() As System.IO.FileInfo Implements isr.Core.IConnectionInfo.DatabaseFileInfo
        Get
            Return _connectionInfo.DatabaseFileInfo
        End Get
    End Property

    ''' <summary>
    ''' Gets the database server instance name.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property InstanceName() As String Implements Core.IConnectionInfo.InstanceName
        Get
            Return _connectionInfo.InstanceName
        End Get
    End Property

    ''' <summary>
    ''' Gets the database server name.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ServerName() As String Implements Core.IConnectionInfo.ServerName
        Get
            Return _connectionInfo.ServerName
        End Get
    End Property

#End Region

#Region " I DATABASE MANAGER "

    Private _lastConnectionFailed As Boolean
    ''' <summary>
    ''' Gets or sets the outcome of last connection attempt.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property LastConnectionFailed() As Boolean Implements IDatabaseManager.LastConnectionFailed
        Get
            Return _lastConnectionFailed
        End Get
        Protected Set(ByVal value As Boolean)
            _lastConnectionFailed = value
        End Set
    End Property

    ''' <summary>
    ''' Pings the database host. If CE, test if database file exists..
    ''' </summary>
    ''' <param name="timeout"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' <example>
    ''' <code>
    ''' </code>
    ''' </example>
    MustOverride Function PingHost(ByVal timeout As Integer) As Boolean Implements IDatabaseManager.PingHost

    ''' <summary>
    ''' Returns true if able to connect to the database using the specified connection string.
    ''' </summary>
    ''' <param name="connectionString">Specifies the connection string to connect to.</param>
    ''' <param name="keepConnectionOpen">True to keep connection open.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public MustOverride Function TryConnect(ByVal connectionString As String, ByVal keepConnectionOpen As Boolean) As Boolean Implements IDatabaseManager.TryConnect

    ''' <summary>
    ''' Returns true if able to connect to the database using the specified connection string.
    ''' </summary>
    ''' <param name="keepConnectionOpen">True to keep connection open.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function TryConnect(ByVal keepConnectionOpen As Boolean) As Boolean Implements IDatabaseManager.TryConnect
        Return Me.TryConnect(Me.ConnectionString, keepConnectionOpen)
    End Function

#End Region

#Region " BACKUP "

    ''' <summary>
    ''' Sets the backup file info using the connection info.
    ''' </summary>
    ''' <param name="info"></param>
    ''' <remarks></remarks>
    Private Sub backupFileInfoSetter(ByVal info As ConnectionInfo)
        _backupFileInfo = New System.IO.FileInfo(System.IO.Path.Combine(info.DatabaseFileInfo.Directory.Parent.FullName, _
                                                    _defaultBackupFolderTitle & "\" & info.DatabaseName & _defaultBackupFileExtension))
    End Sub

    ''' <summary>
    ''' Backs up and archives the database. With CE we must archive (givbe the database a new date file name 
    ''' because there are not incremental in-file backups like in regular SQL.
    ''' </summary>
    ''' <remarks></remarks>
    Public MustOverride Function BackupDatabase() As Boolean Implements IDatabaseManager.BackupDatabase

    Private _backupFileInfo As System.IO.FileInfo
    ''' <summary>
    ''' Gets or sets the  database file information.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overridable Property BackupFileInfo() As System.IO.FileInfo Implements IDatabaseManager.BackupFileInfo
        Get
            Return _backupFileInfo
        End Get
        Set(ByVal value As System.IO.FileInfo)
            _backupFileInfo = value
        End Set
    End Property

    Private _defaultBackupFileExtension As String
    ''' <summary>
    ''' Gets or sets the default backup file extension.
    ''' Updates the file info if a connection string is defined.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property DefaultBackupFileExtension() As String Implements IDatabaseManager.DefaultBackupFileExtension
        Get
            Return _defaultBackupFileExtension
        End Get
        Set(ByVal value As String)
            If _defaultBackupFileExtension <> value Then
                _defaultBackupFileExtension = value
                If _connectionInfo IsNot Nothing Then
                    backupFileInfoSetter(_connectionInfo)
                End If
            End If
        End Set
    End Property

    Private _defaultBackupFolderTitle As String
    ''' <summary>
    ''' Gets or sets the default backup folder title.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property DefaultBackupFolderTitle() As String Implements IDatabaseManager.DefaultBackupFolderTitle
        Get
            Return _defaultBackupFolderTitle
        End Get
        Set(ByVal value As String)
            If _defaultBackupFolderTitle <> value Then
                _defaultBackupFolderTitle = value
                If _connectionInfo IsNot Nothing Then
                    backupFileInfoSetter(_connectionInfo)
                End If
            End If
        End Set
    End Property

    Private _timestampFormat As String
    ''' <summary>
    ''' Gets or sets the time stamp format string.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property TimestampFormat() As String Implements IDatabaseManager.TimestampFormat
        Get
            Return _timestampFormat
        End Get
        Set(ByVal value As String)
            _timestampFormat = value
        End Set
    End Property


#End Region

#Region " COMMAND "

    ''' <summary>
    ''' Executes the specified command and returns the number of rows affected.
    ''' </summary>
    ''' <param name="connectionString">Specifies the connection string.</param>
    ''' <param name="commandText">Specifies a command</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public MustOverride Function ExecuteCommand(ByVal connectionString As String, ByVal commandText As String) As Integer Implements IDatabaseManager.ExecuteCommand

#End Region

#Region " DATABASE "

    ''' <summary>
    ''' Creates a new database
    ''' </summary>
    ''' <param name="connectionString">Specifies the database connection string</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public MustOverride Function CreateDatabase(ByVal connectionString As String) As Boolean Implements IDatabaseManager.CreateDatabase

    ''' <summary>
    ''' Drops (deletes) the current database.
    ''' </summary>
    ''' <remarks></remarks>
    Public MustOverride Function DropDatabase() As Boolean Implements IDatabaseManager.DropDatabase

    ''' <summary>
    ''' Looks for the the database and returns true if exists.  Sets the local cache 
    ''' <see cref="IsDatabaseExists"/> for the condition.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public MustOverride Function LocateDatabase() As Boolean Implements IDatabaseManager.LocateDatabase

    Private _isDatabaseExists As Boolean
    ''' <summary>
    ''' Gets true if the database exists.  The database must be located first
    ''' using <see cref="LocateDatabase"/>.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property IsDatabaseExists() As Boolean Implements IDatabaseManager.IsDatabaseExists
        Get
            Return _isDatabaseExists
        End Get
    End Property

    ''' <summary>
    ''' Sets the sentinel indicating if the database exists.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <remarks></remarks>
    Protected Sub IsDatabaseExistsSetter(ByVal value As Boolean)
        _isDatabaseExists = value
    End Sub


#End Region

#Region " I MESSAGE QUEUING "

    Private _messageQueue As isr.Core.MessageQueue
    ''' <summary>
    ''' Returns all last interface messages.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function DequeueMessages() As String Implements isr.Core.IMessageQueuing.DequeueMessages
        Return _messageQueue.DequeueAll
    End Function

    ''' <summary>
    ''' Enqueues a message.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function EnqueueMessage(ByVal value As String) As String Implements isr.Core.IMessageQueuing.EnqueueMessage
        Return _messageQueue.Enqueue(value)
    End Function

    ''' <summary>
    ''' Enqueues a message.
    ''' </summary>
    ''' <param name="format"></param>
    ''' <param name="args"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function EnqueueMessage(ByVal format As String, ByVal ParamArray args() As Object) As String Implements isr.Core.IMessageQueuing.EnqueueFormat
        Return _messageQueue.Enqueue(format, args)
    End Function

    ''' <summary>
    ''' Clears the queue of interface Messages.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ClearMessages() Implements isr.Core.IMessageQueuing.ClearMessages
        _messageQueue.Clear()
    End Sub

    Public Function HasMessages() As Boolean Implements isr.Core.IMessageQueuing.HasMessages
        Return _messageQueue.HasItems
    End Function

    Public Function GetMessages() As String Implements Core.IMessageQueuing.ToString
        Return _messageQueue.ToString
    End Function

#End Region

#Region " QUERY "

    ''' <summary>
    ''' Executes the specified query command and returns a long integer.
    ''' </summary>
    ''' <param name="connectionString">Specifies the connection string.</param>
    ''' <param name="queryCommand">Specifies a query</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function QueryInt64(ByVal connectionString As String, ByVal queryCommand As String) As Int64 Implements IDatabaseManager.QueryInt64
        Return Me.ExecuteReader(Me.ConnectionString, queryCommand).Reader.GetInt64(0)
    End Function

    ''' <summary>
    ''' Executes the specified query command and returns a long integer.
    ''' </summary>
    ''' <param name="queryCommand">Specifies a query</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function QueryInt64(ByVal queryCommand As String) As Int64 Implements IDatabaseManager.QueryInt64
        Return Me.QueryInt64(Me.ConnectionString, queryCommand)
    End Function

    ''' <summary>
    ''' Reads a string type field from the database using the specified query..
    ''' </summary>
    ''' <param name="connectionString">Specifies the connection string.</param>
    ''' <param name="queryCommand">Specifies a query</param>
    ''' <param name="columnIndex">Specifies the index where the returned value resides.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function QueryString(ByVal connectionString As String, ByVal queryCommand As String, ByVal columnIndex As Integer) As String Implements IDatabaseManager.QueryString

        Using connReader As ConnectedReader = Me.ExecuteReader(connectionString, queryCommand)
            If connReader.Reader.Read Then
                Return connReader.Reader.GetString(columnIndex)
            Else
                Return ""
            End If
        End Using

    End Function

    ''' <summary>
    ''' Reads a string type field from the database using the specified query..
    ''' </summary>
    ''' <param name="connectionString">Specifies the connection string.</param>
    ''' <param name="queryCommand">Specifies a query</param>
    ''' <param name="columnName">Specifies the column name where the returned value resides.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function QueryString(ByVal connectionString As String, ByVal queryCommand As String, ByVal columnName As String) As String Implements IDatabaseManager.QueryString

        Using connReader As ConnectedReader = Me.ExecuteReader(connectionString, queryCommand)
            If connReader.Reader.Read Then
                Dim columnIndex As Integer = connReader.Reader.GetOrdinal(columnName)
                If columnIndex >= 0 Then
                    Return connReader.Reader.GetString(columnIndex)
                Else
                    Return ""
                End If
            Else
                Return ""
            End If
        End Using

    End Function

#End Region

#Region " READER "

    ''' <summary>
    ''' Executes the specified query command and returns reference to the 
    ''' <see cref="ConnectedReader">connected reader</see>
    ''' </summary>
    ''' <param name="connectionString">Specifies the connection string.</param>
    ''' <param name="queryCommand">Specifies a query</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public MustOverride Function ExecuteReader(ByVal connectionString As String, ByVal queryCommand As String) As ConnectedReader Implements IDatabaseManager.ExecuteReader

    ''' <summary>
    ''' Executes the specified query command and returns reference to the 
    ''' <see cref="ConnectedReader">connected reader</see>
    ''' </summary>
    ''' <param name="queryCommand">Specifies a query</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ExecuteReader(ByVal queryCommand As String) As ConnectedReader Implements IDatabaseManager.ExecuteReader
        Return Me.ExecuteReader(Me.ConnectionString, queryCommand)
    End Function

#End Region

#Region " TABLES "

    ''' <summary>
    ''' Returns true if a record exists sutifying the query command.
    ''' </summary>
    ''' <param name="connectionString">Specifies the connection string.</param>
    ''' <param name="queryCommand">Specifies a query</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function RecordExists(ByVal connectionString As String, ByVal queryCommand As String) As Boolean Implements IDatabaseManager.RecordExists

        Using connReader As ConnectedReader = Me.ExecuteReader(connectionString, queryCommand)
            Return connReader.Reader.Read
        End Using

    End Function

    ''' <summary>
    ''' Returns true if the specified table exists.
    ''' </summary>
    ''' <param name="connectionString">Specifies the connection string.</param>
    ''' <param name="value">Specifies the table name</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function TableExists(ByVal connectionString As String, ByVal value As String) As Boolean Implements IDatabaseManager.TableExists

        Dim queryCommand As String = "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '{0}'"
        queryCommand = String.Format(Globalization.CultureInfo.CurrentCulture, queryCommand, value)
        Return RecordExists(connectionString, queryCommand)

    End Function

#End Region

#Region " UPDATE QUERY "

    ''' <summary>
    ''' Gets the number of distinct SQL commands defined in the specified query.
    ''' </summary>
    ''' <param name="query"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function DeriveCommandCount(ByVal query As String) As Integer
        Return isr.Data.DatabaseManagerBase.PrepareCommandQuery(query).Length
    End Function

    ''' <summary>
    ''' Prepares a query for processing as a connection command. 
    ''' If preceeded with '@' the query is retrieved form file.
    ''' Multiple commands must be delimited with a ';' and CR or 
    ''' ';' and CR-LF.
    ''' </summary>
    ''' <param name="query">Specifies a query string or a file name</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function PrepareCommandQuery(ByVal query As String) As String()

        If String.IsNullOrEmpty(query) OrElse query.Length < 2 Then

            Return New String() {""}

        Else

            ' check if using file or query
            If query.Substring(0, 1) = "@" Then
                ' if the is a file name then get the file name
                Dim filePathName As String = query.Substring(1)
                query = My.Computer.FileSystem.ReadAllText(filePathName)
            End If

            ' split queries to lines
            Dim lines As String()
            lines = query.Split(Environment.NewLine.ToCharArray, StringSplitOptions.RemoveEmptyEntries)
            Dim commandList As New List(Of String)
            Dim commandLine As New System.Text.StringBuilder
            For Each line As String In lines
                isr.Core.TextExtensions.AppendLine(commandLine, line)
                If line.Trim.EndsWith(";", StringComparison.OrdinalIgnoreCase) OrElse _
                   line.Trim.EndsWith("GO", StringComparison.OrdinalIgnoreCase) Then
                    commandList.Add(commandLine.ToString)
                    commandLine = New System.Text.StringBuilder
                End If
            Next
            Return commandList.ToArray

        End If

    End Function

    ''' <summary>
    ''' Executes a table update query.
    ''' </summary>
    ''' <param name="query">Specifies the query or file name where the query is located.
    ''' A file name begins with '@'</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function UpdateTableQuery(ByVal query As String) As Integer Implements IDatabaseManager.UpdateTableQuery
        Return UpdateTableQuery(Me.ConnectionString, query)
    End Function

    ''' <summary>
    ''' Executes a table update query.
    ''' </summary>
    ''' <param name="query">Specifies the query lines.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function UpdateTableQuery(ByVal query As String()) As Integer Implements IDatabaseManager.UpdateTableQuery
        Return UpdateTableQuery(Me.ConnectionString, query)
    End Function

    Private _queryErrorCount As Integer
    ''' <summary>
    ''' Holds the number of update queries that failed.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property QueryErrorCount() As Integer Implements IDatabaseManager.QueryErrorCount
        Get
            Return _queryErrorCount
        End Get
    End Property

    ''' <summary>
    ''' Sets the query error count.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <remarks></remarks>
    Protected Sub QueryErrorCountSetter(ByVal value As Integer)
        _queryErrorCount = value
    End Sub

    Private _queryUpdateCount As Integer
    ''' <summary>
    ''' Holds the number of update query that worked.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property QueryUpdateCount() As Integer Implements IDatabaseManager.QueryUpdateCount
        Get
            Return _queryUpdateCount
        End Get
    End Property

    ''' <summary>
    ''' Sets the query update count.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <remarks></remarks>
    Protected Sub QueryUpdateCountSetter(ByVal value As Integer)
        _queryUpdateCount = value
    End Sub

    ''' <summary>
    '''  Executes a table update query.
    ''' </summary>
    ''' <returns>The total number of minor errors minus the number of successful command executions</returns>
    ''' <remarks></remarks>
    ''' <param name="query">Specifies the query or file name where the query is located.
    ''' A file name begins with '@'</param>
    ''' <param name="connectionString">Specifies the database connection string</param>
    ''' <param name="ignoreMinorErrors">Allows ignoring minor SQL error on a complex multi-line command.</param>
    Public MustOverride Function UpdateTableQuery(ByVal connectionString As String, ByVal query As String, ByVal ignoreMinorErrors As Boolean) As Integer Implements IDatabaseManager.UpdateTableQuery

    ''' <summary>
    '''  Executes a table update query.
    ''' </summary>
    ''' <returns>The total number of minor errors minus the number of successful command executions</returns>
    ''' <remarks></remarks>
    ''' <param name="query">Specifies the query lines</param>
    ''' <param name="connectionString">Specifies the database connection string</param>
    ''' <param name="ignoreMinorErrors">Allows ignoring minor SQL error on a complex multi-line command.</param>
    Public MustOverride Function UpdateTableQuery(ByVal connectionString As String, ByVal query As String(), ByVal ignoreMinorErrors As Boolean) As Integer Implements IDatabaseManager.UpdateTableQuery

    ''' <summary>
    '''  Executes a table update query.
    ''' </summary>
    ''' <param name="query">Specifies the query of file name where the query is located.
    ''' A file name begins with '@'</param>
    ''' <param name="connectionString">Specifies the database connection string</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function UpdateTableQuery(ByVal connectionString As String, ByVal query As String) As Integer Implements IDatabaseManager.UpdateTableQuery
        Dim recordCount As Int32
        If String.IsNullOrEmpty(query) Then
            Return recordCount
        Else
            recordCount = UpdateTableQuery(connectionString, query, False)
        End If
    End Function

    ''' <summary>
    '''  Executes a table update query.
    ''' </summary>
    ''' <param name="query">Specifies the query lines.</param>
    ''' <param name="connectionString">Specifies the database connection string</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function UpdateTableQuery(ByVal connectionString As String, ByVal query As String()) As Integer Implements IDatabaseManager.UpdateTableQuery
        Dim recordCount As Int32
        If query Is Nothing Then
            Return recordCount
        Else
            recordCount = UpdateTableQuery(connectionString, query, False)
        End If
    End Function

#End Region

#Region " VERIFICATION "

    ''' <summary>
    ''' Gets teh sentinel indicating a failed release.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Const FailedRelease As String = "<failed>"

    Private _currentReleaseVersion As String
    ''' <summary>
    ''' Gets the current version. Reads from the database if the stored version is null or faile reading the version or last
    ''' verification failed.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CurrentReleaseVersion() As String Implements IDatabaseManager.CurrentReleaseVersion
        Get
            Return _currentReleaseVersion
        End Get
        Protected Set(ByVal value As String)
            _currentReleaseVersion = value
        End Set
    End Property

    ''' <summary>
    ''' Sets the current relerase version query text.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <remarks></remarks>
    Protected Sub CurrentReleaseVersionQueryTextSetter(ByVal value As String)
        _currentReleaseVersionQueryText = value
    End Sub

    Private _currentReleaseVersionQueryText As String
    ''' <summary>
    ''' Gets or sets the text used to query the current release. 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CurrentReleaseVersionQueryText() As String Implements IDatabaseManager.CurrentReleaseVersionQueryText
        Get
            Return _currentReleaseVersionQueryText
        End Get
        Set(ByVal value As String)
            _currentReleaseVersionQueryText = value
        End Set
    End Property

    Private _databaseVerified As Boolean
    ''' <summary>
    ''' Returns true if the database was successfuly verified.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property DatabaseVerified() As Boolean Implements IDatabaseManager.DatabaseVerified
        Get
            Return _databaseVerified
        End Get
        Protected Set(ByVal value As Boolean)
            _databaseVerified = value
        End Set
    End Property

    ''' <summary>
    ''' Reads the current version.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CurrentReleaseVersionGetter(ByVal connectionString As String) As String Implements IDatabaseManager.CurrentReleaseVersionGetter
        Return Me.QueryString(connectionString, Me.CurrentReleaseVersionQueryText, 0)
    End Function

    ''' <summary>
    ''' Verifies that the local database is connectible and is current.
    ''' </summary>
    ''' <param name="connectionString">Specifies the database connection string</param>
    ''' <param name="version">Specifies the database version</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function VerifyDatabaseVersion(ByVal connectionString As String, ByVal version As String) As Boolean Implements IDatabaseManager.VerifyDatabaseVersion

        ' clear the verification status.
        Me.DatabaseVerified = False

        ' set default properties.
        If TryConnect(connectionString, True) Then
            Me.CurrentReleaseVersion = Me.CurrentReleaseVersionGetter(connectionString)
            Me.DatabaseVerified = System.Version.Equals(New System.Version(Me.CurrentReleaseVersion), New System.Version(version))
            If Not Me.DatabaseVerified Then
                Me.EnqueueMessage("Local database version mismatch. Expected version '{0}' and found '{1}'.", version, Me.CurrentReleaseVersion)
            End If
        Else
            Me.EnqueueMessage("Local database not found using the connection string '{0}'.", connectionString)
        End If
        Return Me.DatabaseVerified

    End Function

#End Region

End Class

