﻿Namespace Sql
    ''' <summary>
    ''' Implements a database manager entity for SQL Compact Edition.</summary>
    ''' <license>
    ''' (c) 2009 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="10/21/2009" by="David Hary" revision="1.2.3581.x">
    ''' created.
    ''' </history>
    ''' <history date="09/16/2011" by="David Hary" revision="1.2.4276.x">
    ''' Use namespace isr.Data.Compact.
    ''' </history>
    Public Class DatabaseManager

        Inherits DatabaseManagerBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>
        ''' Construtor for this class.
        ''' </summary>
        ''' <param name="connectionString">Specifies the connection string.</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal connectionString As String)
            MyBase.New(connectionString)
            CurrentReleaseVersionQueryTextSetter("SELECT ReleaseRevision, IsCurrentRelease FROM ReleaseHistory WHERE (IsCurrentRelease = 1)")
        End Sub

#End Region

#Region " CONNECTION "

        ''' <summary>
        ''' Pings the database server.
        ''' </summary>
        ''' <param name="timeout"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overrides Function PingHost(ByVal timeout As Integer) As Boolean
            Return My.Computer.Network.Ping(Me.ServerName, timeout)
        End Function

#End Region

#Region " I DATABASE MANAGER "

        ''' <summary>
        ''' Returns true if able to connect to the database using the specified connection string.
        ''' </summary>
        ''' <param name="connectionString">Specifies the connection string to connect to.</param>
        ''' <param name="keepConnectionOpen">True to keep connection open.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overrides Function TryConnect(ByVal connectionString As String, ByVal keepConnectionOpen As Boolean) As Boolean

            If String.IsNullOrEmpty(connectionString) Then
                Throw New ArgumentNullException("connectionString")
            End If

            Dim conn As System.Data.SqlClient.SqlConnection = Nothing
            Try

                ' connect to the local database.
                conn = New System.Data.SqlClient.SqlConnection(connectionString)
                conn.Open()
                MyBase.LastConnectionFailed = conn.State <> ConnectionState.Open
                Return Not MyBase.LastConnectionFailed

            Catch ex As System.Data.SqlClient.SqlException
                MyBase.EnqueueMessage("Exception occurred attempting to connect using connection string '{0}'. Details: {1}{2}.", _
                                      connectionString, Environment.NewLine, ex)
            Finally
                If conn IsNot Nothing Then
                    If Not keepConnectionOpen Then
                        If conn.State = ConnectionState.Open Then
                            conn.Close()
                        End If
                        conn.Dispose()
                    End If
                End If
            End Try
            Return False

        End Function

#End Region

#Region " COMMAND "

        ''' <summary>
        ''' Executes the specified command and returns the number of rows affected.
        ''' </summary>
        ''' <param name="connectionString">Specifies the connection string.</param>
        ''' <param name="commandText">Specifies a command</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overrides Function ExecuteCommand(ByVal connectionString As String, ByVal commandText As String) As Integer

            Dim rowCount As Integer = 0
            Using connection As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(connectionString)
                Dim sqlCommand As System.Data.SqlClient.SqlCommand = connection.CreateCommand()
                sqlCommand.CommandText = commandText
                connection.Open()
                rowCount = sqlCommand.ExecuteNonQuery()
                connection.Close()
            End Using
            Return rowCount

        End Function

#End Region

#Region " BACKUP "

        ''' <summary>
        ''' Backup the specified database on the local server instance.  A differential backup is 
        ''' done if the backup already exists.
        ''' </summary>
        ''' <param name="instanceName">Specifies the local server instance name, such as 'SQLEXPRESS'</param>
        ''' <param name="databaseName">Specifies the database name.</param>
        ''' <param name="backupFilePathName">Specifies the file name path of the database backup.</param>
        ''' <param name="backupName">Specifies the backup name.</param>
        ''' <returns>The backup file path name for the database file.</returns>
        ''' <remarks></remarks>
        Public Shared Function BackupLocalDatabase(ByVal instanceName As String, _
            ByVal databaseName As String, _
            ByVal backupFilePathName As String, ByVal backupName As String) As String

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            If String.IsNullOrEmpty(databaseName) Then
                Throw New ArgumentNullException("databaseName")
            End If

            If String.IsNullOrEmpty(backupFilePathName) Then
                Throw New ArgumentNullException("backupFilePathName")
            End If

            If String.IsNullOrEmpty(backupName) Then
                Throw New ArgumentNullException("backupName")
            End If

            Dim lastAction As String
            Dim cmd As System.Data.SqlClient.SqlCommand = Nothing
            Dim conn As System.Data.SqlClient.SqlConnection = Nothing

            Try

                ' connect to the master database.
                conn = DatabaseManager.ConnectLocalDatabase(instanceName, "master")

                Const fullBackupSyntax As String = "BACKUP DATABASE [{0}] TO DISK = '{1}' WITH FORMAT, NAME = '{2}'"
                Const diffBackupSyntax As String = "BACKUP DATABASE [{0}] TO DISK = '{1}' WITH DIFFERENTIAL"

                Dim isDifferential As Boolean = System.IO.File.Exists(backupFilePathName)
                Dim query As String
                ' check if database file exists
                If isDifferential Then
                    lastAction = "Differential "
                    query = String.Format(Globalization.CultureInfo.CurrentCulture, _
                        diffBackupSyntax, databaseName, backupFilePathName)
                Else
                    lastAction = "Full "
                    query = String.Format(Globalization.CultureInfo.CurrentCulture, _
                        fullBackupSyntax, databaseName, backupFilePathName, backupName)
                End If
                lastAction = lastAction & "backup of database: [{0}] & to '{1}'."
                lastAction = String.Format(Globalization.CultureInfo.CurrentCulture, lastAction, databaseName, backupFilePathName)

                lastAction = "Created query on the local SQL server:"
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
                My.Application.Log.WriteEntry(query, TraceEventType.Verbose)

                lastAction = "Creating command on the local SQL server..."
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)

                ' Create SQL query 
                cmd = New System.Data.SqlClient.SqlCommand(query, conn)
                If cmd.Connection.State <> ConnectionState.Open Then
                    cmd.Connection.Open()
                End If
                cmd.CommandType = System.Data.CommandType.Text

                ' return the main path name
                BackupLocalDatabase = backupFilePathName

                lastAction = "Created command to execute on the local SQL server:"
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
                My.Application.Log.WriteEntry(cmd.CommandText, TraceEventType.Verbose)

                lastAction = "Executing command on the local SQL server..."
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
                cmd.ExecuteNonQuery()

                My.Application.Log.WriteEntry("Done database backup.", TraceEventType.Verbose)

            Finally

                If Not cmd Is Nothing Then
                    cmd.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If

            End Try

        End Function

        ''' <summary>
        ''' Backs up the database.
        ''' </summary>
        ''' <remarks></remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
        Public Overrides Function BackupDatabase() As Boolean

            Dim lastAction As String = String.Empty

            Try

                Dim backupName As String = MyBase.DatabaseName & "_Full_Backup"
                lastAction = "Backing up database on the local SQL server."
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
                Dim actual As String = DatabaseManager.BackupLocalDatabase(MyBase.InstanceName, MyBase.DatabaseName, MyBase.BackupFileInfo.FullName, backupName)
                If String.IsNullOrEmpty(actual) Then

                    lastAction = "Failed backing up database on the local SQL server."
                    My.Application.Log.WriteEntry(lastAction, TraceEventType.Warning)
                    Return False

                Else

                    lastAction = "Database backed up successfully."
                    My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
                    Return True

                End If

            Catch ex As System.Exception

                My.Application.Log.WriteException(ex, TraceEventType.Error, lastAction)
                Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "{0} failed {1}: {2}.", _
                    Me, lastAction, ex)
                MyBase.EnqueueMessage(message)
                Return False

            End Try

        End Function

#End Region

#Region " SERVER MANAGEMENT "

        ''' <summary>
        ''' Connects to the local server instance and returns an instance of this server object.
        ''' </summary>
        ''' <param name="instanceName">Specifies the instance name to connect to.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function ConnectLocalServer(ByVal instanceName As String) As Microsoft.SqlServer.Management.Smo.Server

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            Dim conn As System.Data.SqlClient.SqlConnection = Nothing
            ' connect to the master database.
            conn = DatabaseManager.ConnectLocalDatabase(instanceName, "master")

            Dim svrConnection As New Microsoft.SqlServer.Management.Common.ServerConnection(conn)
            Dim svr As New Microsoft.SqlServer.Management.Smo.Server(svrConnection)
            Return svr

        End Function

#End Region

#Region " DATABASE "

        ''' <summary>
        ''' Connects to the specified database on the specified local server instance.
        ''' </summary>
        ''' <param name="instanceName">Specifies the server instance name, such as 'SQLEXPRESS'</param>
        ''' <param name="databaseName"></param>
        ''' <remarks></remarks>
        Public Shared Function ConnectLocalDatabase(ByVal instanceName As String, _
            ByVal databaseName As String) As System.Data.SqlClient.SqlConnection

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            If String.IsNullOrEmpty(databaseName) Then
                Throw New ArgumentNullException("databaseName")
            End If

            ' check if we have a local SQL server.
            Dim server As New Microsoft.SqlServer.Management.Smo.Server(System.Environment.MachineName)
            If server Is Nothing Then
                Throw New LocalServerNotFoundException()
            End If

            Dim conn As System.Data.SqlClient.SqlConnection = Nothing

            ' Get the connection string to the master catalog
            Dim connString As String = ConnectionInfo.BuildLocalConnectionString(instanceName, databaseName)

            My.Application.Log.WriteEntry("Created connection string to local database: ", TraceEventType.Verbose)
            My.Application.Log.WriteEntry(connString, TraceEventType.Verbose)
            conn = New System.Data.SqlClient.SqlConnection(connString)
            If conn.State <> System.Data.ConnectionState.Open Then
                My.Application.Log.WriteEntry("Connecting to database", TraceEventType.Verbose)
                conn.Open()
            End If
            My.Application.Log.WriteEntry("Connected to database", TraceEventType.Verbose)

            Return conn

        End Function

        ''' <summary>
        ''' Not implemented yet.
        ''' </summary>
        ''' <param name="connectionString"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overrides Function CreateDatabase(ByVal connectionString As String) As Boolean
            Return False
        End Function

        ''' <summary>
        ''' Drops the current database.
        ''' </summary>
        ''' <remarks></remarks>
        Public Overrides Function DropDatabase() As Boolean

            Dim lastAction As String = String.Empty

            lastAction = "Dropping database."
            My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
            DatabaseManager.DropLocalDatabase(MyBase.InstanceName, MyBase.DatabaseName)
            Return True

        End Function

        ''' <summary>
        ''' Returns the collection of databases in the specified server.
        ''' </summary>
        ''' <param name="connectionString">Specifies the connection string to connect to.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function EnumerateDatabases(ByVal connectionString As String) As Microsoft.SqlServer.Management.Smo.DatabaseCollection

            If String.IsNullOrEmpty(connectionString) Then
                Throw New ArgumentNullException("connectionString")
            End If

            Using conn As New System.Data.SqlClient.SqlConnection(connectionString)

                ' connect to the master database.
                conn.ConnectionString = conn.ConnectionString.Replace(conn.Database, "master")

                Dim svrConnection As New Microsoft.SqlServer.Management.Common.ServerConnection(conn)
                Dim svr As New Microsoft.SqlServer.Management.Smo.Server(svrConnection)
                Return svr.Databases

            End Using

        End Function

        ''' <summary>
        ''' Returns the collection of databases in the specified server.
        ''' </summary>
        ''' <param name="instanceName">Specifies the instance name to connect to.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function EnumerateLocalDatabases(ByVal instanceName As String) As Microsoft.SqlServer.Management.Smo.DatabaseCollection

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            Dim conn As System.Data.SqlClient.SqlConnection = Nothing
            Try

                ' connect to the master database.
                conn = DatabaseManager.ConnectLocalDatabase(instanceName, "master")

                Dim svrConnection As New Microsoft.SqlServer.Management.Common.ServerConnection(conn)
                Dim svr As New Microsoft.SqlServer.Management.Smo.Server(svrConnection)
                Return svr.Databases

            Finally

                If Not conn Is Nothing Then
                    conn.Dispose()
                End If

            End Try

        End Function

        ''' <summary>
        ''' Removes the database from the SQL server and returns a comma separated list
        ''' of the database and log file names.
        ''' </summary>
        ''' <param name="instanceName"></param>
        ''' <param name="catalogName"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function DropLocalDatabase(ByVal instanceName As String, _
          ByVal catalogName As String) As String

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            If String.IsNullOrEmpty(catalogName) Then
                Throw New ArgumentNullException("catalogName")
            End If

            Dim databases As Microsoft.SqlServer.Management.Smo.DatabaseCollection = DatabaseManager.EnumerateLocalDatabases(instanceName)

            If databases Is Nothing OrElse databases.Count <= 0 Then
                My.Application.Log.WriteEntry("Databases not found.", TraceEventType.Critical)
            End If

            Dim database As Microsoft.SqlServer.Management.Smo.Database
            If databases.Contains(catalogName) Then

                database = databases.Item(catalogName)
                DropLocalDatabase = database.FileGroups(0).Files(0).FileName & "," _
                    & database.LogFiles(0).FileName
                database.Drop()

            Else

                My.Application.Log.WriteEntry("Failed dropping database because the database does not exist.  System reports the following databases:", TraceEventType.Critical)
                For Each database In databases
                    My.Application.Log.WriteEntry(database.Name, TraceEventType.Critical)
                Next
                Return "Database not found"

            End If

        End Function

        ''' <summary>
        ''' Returns true if a database exists on the local server instance.
        ''' </summary>
        ''' <param name="connectionString">Specifies the connection string.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function IsCatalogExists(ByVal connectionString As String) As Boolean

            If String.IsNullOrEmpty(connectionString) Then
                Throw New ArgumentNullException("connectionString")
            End If

            Dim affirmative As Boolean
            Using conn As New System.Data.SqlClient.SqlConnection(connectionString)

                Dim catalogName As String = conn.Database

                Dim databases As Microsoft.SqlServer.Management.Smo.DatabaseCollection = DatabaseManager.EnumerateDatabases(connectionString)

                If databases Is Nothing OrElse databases.Count <= 0 Then
                    My.Application.Log.WriteEntry("Databases not found.", TraceEventType.Critical)
                    Return False
                End If

                affirmative = databases.Contains(catalogName)

                If Not affirmative Then
                    ' if not located, report all the server that were found
                    My.Application.Log.WriteEntry("Failed locating database.  System reports the following databases:", TraceEventType.Critical)
                    For Each database As Microsoft.SqlServer.Management.Smo.Database In databases
                        My.Application.Log.WriteEntry(database.Name, TraceEventType.Critical)
                    Next
                End If

            End Using
            Return affirmative

        End Function

        ''' <summary>
        ''' Looks for the the database and returns true if exists.  Sets the local cache 
        ''' <see cref="IsDatabaseExists"/> for the condition.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overrides Function LocateDatabase() As Boolean

            MyBase.IsDatabaseExistsSetter(DatabaseManager.IsCatalogExists(ConnectionString))
            Return MyBase.IsDatabaseExists

        End Function

#End Region

#Region " QUERY "

#If False Then
  ''' <summary>
  ''' Reads a string type field from the database using the specified query..
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function QueryString(ByVal connectionString As String, ByVal queryText As String) As String
    Using connection As New System.Data.SqlClient.SqlConnection(connectionString)
      Dim command As New System.Data.SqlClient.SqlCommand()
      command.Connection = connection
      command.CommandTimeout = 15
      command.CommandType = System.Data.CommandType.Text
      command.CommandText = queryText
      command.Connection.Open()
      Dim reader As System.Data.SqlClient.SqlDataReader = command.ExecuteReader( _
                                                            System.Data.CommandBehavior.CloseConnection _
                                                            Or System.Data.CommandBehavior.SingleRow)
      Try
        If reader.HasRows Then
          reader.Read()
          Return reader.GetString(0)
        Else
          Return FailedRelease
        End If
      Finally
        reader.Close()
        command.Connection.Close()
      End Try

    End Using

  End Function
#End If

#End Region

#Region " READER "

        ''' <summary>
        ''' Executes the specified query command and returns reference to the 
        ''' <see cref="ConnectedReader">connected reader</see>
        ''' </summary>
        ''' <param name="connectionString">Specifies the connection string.</param>
        ''' <param name="queryCommand">Specifies a query</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overrides Function ExecuteReader(ByVal connectionString As String, ByVal queryCommand As String) As ConnectedReader

            Return ExecuteReader(New System.Data.SqlClient.SqlConnection(connectionString), queryCommand)

        End Function

        ''' <summary>
        ''' Executes the specified query command and returns reference to the 
        ''' <see cref="ConnectedReader">connected reader</see>
        ''' </summary>
        ''' <param name="connection">Specifies a <see cref="System.Data.SqlClient.SqlConnection">connection</see>.</param>
        ''' <param name="queryCommand">Specifies a query</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overloads Shared Function ExecuteReader(ByVal connection As System.Data.SqlClient.SqlConnection, ByVal queryCommand As String) As ConnectedReader

            Dim command As New System.Data.SqlClient.SqlCommand()
            command.Connection = connection
            command.CommandTimeout = 15
            command.CommandType = System.Data.CommandType.Text
            command.CommandText = queryCommand
            If connection.State <> ConnectionState.Open Then
                command.Connection.Open()
            End If
            Dim reader As System.Data.SqlClient.SqlDataReader = command.ExecuteReader( _
                                                                  System.Data.CommandBehavior.CloseConnection)
            Return New ConnectedReader(connection, reader)

        End Function

#End Region

#Region " TABLES "

#If False Then

  ''' <summary>
  ''' Returns true if a record exists sutifying the query command.
  ''' </summary>
  ''' <param name="connectionString">Specifies the connection string.</param>
  ''' <param name="queryCommand">Specifies a query</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function RecordExists(ByVal connectionString As String, ByVal queryCommand As String) As Boolean

    Dim connReader As ConnectedReader = Me.ExecuteReader(connectionString, queryCommand)
    RecordExists = connReader.Reader.Read
    connReader.CloseConnection()

  End Function

#End If

#End Region

#Region " UPDATE QUERY "

        ''' <summary>
        '''  Executes a table update query.
        ''' </summary>
        ''' <returns>The total number of minor errors minus the number of successful command executions</returns>
        ''' <remarks>If using multiple SQL commands, each command must end with a ';' followed by a new line.</remarks>
        ''' <param name="query">Specifies the query of file name where the query is located.
        ''' A file name begins with '@'</param>
        ''' <param name="connectionString">Specifies the database connection string</param>
        ''' <param name="ignoreMinorErrors">Allows ignoring minor SQL error on a complex multi-line command.</param>
        Public Overrides Function UpdateTableQuery(ByVal connectionString As String, ByVal query As String, ByVal ignoreMinorErrors As Boolean) As Integer
            Return UpdateTableQuery(connectionString, DatabaseManagerBase.PrepareCommandQuery(query), ignoreMinorErrors)
        End Function

        ''' <summary>
        '''  Executes a table update query.
        ''' </summary>
        ''' <returns>The total number of minor errors minus the number of successful command executions</returns>
        ''' <remarks>If using multiple SQL commands, each command must end with a ';' followed by a new line.</remarks>
        ''' <param name="query">Specifies the query of file name where the query is located.
        ''' A file name begins with '@'</param>
        ''' <param name="connectionString">Specifies the database connection string</param>
        ''' <param name="ignoreMinorErrors">Allows ignoring minor SQL error on a complex multi-line command.</param>
        Public Overrides Function UpdateTableQuery(ByVal connectionString As String, ByVal query As String(), ByVal ignoreMinorErrors As Boolean) As Integer
            Using connection As New System.Data.SqlClient.SqlConnection(connectionString)
                Return Me.UpdateTableQuery(connection, query, ignoreMinorErrors)
            End Using
        End Function

        ''' <summary>
        '''  Executes a table update query.
        ''' </summary>
        ''' <returns>The total number of minor errors minus the number of successful command executions</returns>
        ''' <remarks>If using multiple SQL commands, each command must end with a ';' followed by a new line.</remarks>
        ''' <param name="query">Specifies the query of file name where the query is located.
        ''' A file name begins with '@'</param>
        ''' <param name="connection">Specifies the database connection</param>
        ''' <param name="ignoreMinorErrors">Allows ignoring minor SQL error on a complex multi-line command.</param>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
        Public Overloads Function UpdateTableQuery(ByVal connection As System.Data.SqlClient.SqlConnection, ByVal query As String(), ByVal ignoreMinorErrors As Boolean) As Integer

            Dim recordCount As Int32
            Dim isOpened As Boolean

            MyBase.QueryUpdateCountSetter(0)
            MyBase.QueryErrorCountSetter(0)
            If Not query Is Nothing Then
                Dim command As New System.Data.SqlClient.SqlCommand()
                command.Connection = connection
                command.CommandType = System.Data.CommandType.Text
                If connection.State <> ConnectionState.Open Then
                    isOpened = True
                    connection.Open()
                End If
                For Each queryCommand As String In query
                    If Not String.IsNullOrEmpty(queryCommand) Then
                        command.CommandText = queryCommand
                        If ignoreMinorErrors Then
                            Try
                                'todo: Check on this.
                                ' each update returns -1 if success or and error  if failed.
                                recordCount += command.ExecuteNonQuery()
                                MyBase.QueryUpdateCountSetter(MyBase.QueryUpdateCount + 1)
                            Catch
                                MyBase.QueryErrorCountSetter(MyBase.QueryErrorCount + 1)
                            Finally
                            End Try
                        Else
                            recordCount += command.ExecuteNonQuery()
                            MyBase.QueryUpdateCountSetter(MyBase.QueryUpdateCount + 1)
                        End If
                    End If
                Next
                If isOpened Then
                    connection.Close()
                End If
            End If
            Return recordCount
        End Function

#End Region

    End Class
End Namespace
