﻿''' <summary>
''' Interface for implemenitng a database connection with an open reader.</summary>
''' <license>
''' (c) 2009 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/21/2009" by="David Hary" revision="1.2.3581.x">
''' created.
''' </history>
Public Interface IConnectedReader

  ''' <summary>
  ''' Gets the connection.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  ReadOnly Property Connection() As IDbConnection

  ''' <summary>
  ''' Gets the reader.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  ReadOnly Property Reader() As IDataReader

  ''' <summary>
  ''' Closes the read and the connection.
  ''' </summary>
  ''' <remarks></remarks>
  Sub CloseConnection()

End Interface
