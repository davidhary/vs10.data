﻿''' <summary>
''' Interface for implemenitng a database server manager.</summary>
''' <license>
''' (c) 2009 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/21/2009" by="David Hary" revision="1.2.3581.x">
''' created.
''' </history>
Public Interface IDatabaseManager

    Inherits isr.Core.IMessageQueuing, isr.Core.IConnectionInfo

#Region " CONNECTION "

    ''' <summary>
    ''' Gets or sets the outcome of last connection attempt.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property LastConnectionFailed() As Boolean

    ''' <summary>
    ''' Returns true if able to connect to the database using the specified connection string.
    ''' </summary>
    ''' <param name="connectionString">Specifies the connection string to connect to.</param>
    ''' <param name="keepConnectionOpen">True to keep connection open.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function TryConnect(ByVal connectionString As String, ByVal keepConnectionOpen As Boolean) As Boolean

    ''' <summary>
    ''' Returns true if able to connect to the database using the specified connection string.
    ''' </summary>
    ''' <param name="keepConnectionOpen">True to keep connection open.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function TryConnect(ByVal keepConnectionOpen As Boolean) As Boolean

    ''' <summary>
    ''' Pings the database host. If CE, test if database file exists..
    ''' </summary>
    ''' <param name="timeout"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function PingHost(ByVal timeout As Integer) As Boolean

#End Region

#Region " BACKUP "

    ''' <summary>
    ''' Backs up and archives the database. With CE we must archive (givbe the database a new date file name 
    ''' because there are not incremental in-file backups like in regular SQL.
    ''' </summary>
    ''' <remarks></remarks>
    Function BackupDatabase() As Boolean

    ''' <summary>
    ''' Gets or sets the  database file information.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property BackupFileInfo() As System.IO.FileInfo

    ''' <summary>
    ''' Gets or sets the time stamp format string.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property TimestampFormat() As String

    ''' <summary>
    ''' Gets or sets the default backup folder title.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property DefaultBackupFolderTitle() As String

    ''' <summary>
    ''' Gets or sets the default backup file extension.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property DefaultBackupFileExtension() As String

#End Region

#Region " COMMAND "

    ''' <summary>
    ''' Executes the specified command and returns the number of rows affected.
    ''' </summary>
    ''' <param name="connectionString">Specifies the connection string.</param>
    ''' <param name="commandText">Specifies a command</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ExecuteCommand(ByVal connectionString As String, ByVal commandText As String) As Integer

#End Region

#Region " DATABASE "

    ''' <summary>
    ''' Creates a new database
    ''' </summary>
    ''' <param name="connectionString">Specifies the database connection string</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function CreateDatabase(ByVal connectionString As String) As Boolean

    ''' <summary>
    ''' Drops (deletes) the current database.
    ''' </summary>
    ''' <remarks></remarks>
    Function DropDatabase() As Boolean

    ''' <summary>
    ''' Looks for the the database and returns true if exists.  Sets the local cache 
    ''' <see cref="IsDatabaseExists"/> for the condition.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function LocateDatabase() As Boolean

    ''' <summary>
    ''' Gets true if the database exists.  The database must be located first
    ''' using <see cref="LocateDatabase"/>.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ReadOnly Property IsDatabaseExists() As Boolean

#End Region

#Region " QUERY "

    ''' <summary>
    ''' Reads a string type field from the database using the specified query..
    ''' </summary>
    ''' <param name="connectionString">Specifies the connection string.</param>
    ''' <param name="queryCommand">Specifies a query</param>
    ''' <param name="columnIndex">Specifies the index where the returned value resides.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function QueryString(ByVal connectionString As String, ByVal queryCommand As String, ByVal columnIndex As Integer) As String

    ''' <summary>
    ''' Reads a string type field from the database using the specified query..
    ''' </summary>
    ''' <param name="connectionString">Specifies the connection string.</param>
    ''' <param name="queryCommand">Specifies a query</param>
    ''' <param name="columnName">Specifies the column name where the returned value resides.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function QueryString(ByVal connectionString As String, ByVal queryCommand As String, ByVal columnName As String) As String

    ''' <summary>
    ''' Executes the specified query command and returns a long integer.
    ''' </summary>
    ''' <param name="queryCommand">Specifies a query</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function QueryInt64(ByVal connectionString As String, ByVal queryCommand As String) As Int64

    ''' <summary>
    ''' Executes the specified query command and returns a long integer.
    ''' </summary>
    ''' <param name="queryCommand">Specifies a query</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function QueryInt64(ByVal queryCommand As String) As Int64

#End Region

#Region " READER "

    ''' <summary>
    ''' Executes the specified query command and returns reference to the 
    ''' <see cref="ConnectedReader">connected reader</see>
    ''' </summary>
    ''' <param name="connectionString">Specifies the connection string.</param>
    ''' <param name="queryCommand">Specifies a query</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ExecuteReader(ByVal connectionString As String, ByVal queryCommand As String) As ConnectedReader

    ''' <summary>
    ''' Executes the specified query command and returns reference to the 
    ''' <see cref="ConnectedReader">connected reader</see>
    ''' </summary>
    ''' <param name="queryCommand">Specifies a query</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ExecuteReader(ByVal queryCommand As String) As ConnectedReader

#End Region

#Region " TABLES "

    ''' <summary>
    ''' Returns true if a record exists sutifying the query command.
    ''' </summary>
    ''' <param name="connectionString">Specifies the connection string.</param>
    ''' <param name="queryCommand">Specifies a query</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function RecordExists(ByVal connectionString As String, ByVal queryCommand As String) As Boolean

    ''' <summary>
    ''' Returns true if the specified table exists.
    ''' </summary>
    ''' <param name="connectionString">Specifies the connection string.</param>
    ''' <param name="value">Specifies the table name</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function TableExists(ByVal connectionString As String, ByVal value As String) As Boolean

#End Region

#Region " UPDATE QUERY "

    ''' <summary>
    ''' Executes a table update query.
    ''' </summary>
    ''' <param name="query">Specifies the query of file name where the query is located.
    ''' A file name begins with '@'</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function UpdateTableQuery(ByVal query As String) As Integer

    ''' <summary>
    ''' Executes a table update query.
    ''' </summary>
    ''' <param name="query">Specifies the query lines.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function UpdateTableQuery(ByVal query As String()) As Integer

    ''' <summary>
    ''' Holds the number of update queries that failed.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ReadOnly Property QueryErrorCount() As Integer

    ''' <summary>
    ''' Holds the number of update query that worked.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ReadOnly Property QueryUpdateCount() As Integer

    ''' <summary>
    '''  Executes a table update query.
    ''' </summary>
    ''' <returns>The total number of minor errors minus the number of successful command executions</returns>
    ''' <remarks></remarks>
    ''' <param name="query">Specifies the query of file name where the query is located.
    ''' A file name begins with '@'</param>
    ''' <param name="connectionString">Specifies the database connection string</param>
    ''' <param name="ignoreMinorErrors">Allows ignoring minor SQL error on a complex multi-line command.</param>
    Function UpdateTableQuery(ByVal connectionString As String, ByVal query As String, ByVal ignoreMinorErrors As Boolean) As Integer

    ''' <summary>
    '''  Executes a table update query.
    ''' </summary>
    ''' <returns>The total number of minor errors minus the number of successful command executions</returns>
    ''' <remarks></remarks>
    ''' <param name="query">Specifies the query lines.</param>
    ''' <param name="connectionString">Specifies the database connection string</param>
    ''' <param name="ignoreMinorErrors">Allows ignoring minor SQL error on a complex multi-line command.</param>
    Function UpdateTableQuery(ByVal connectionString As String, ByVal query As String(), ByVal ignoreMinorErrors As Boolean) As Integer

    ''' <summary>
    '''  Executes a table update query.
    ''' </summary>
    ''' <param name="query">Specifies the query of file name where the query is located.
    ''' A file name begins with '@'</param>
    ''' <param name="connectionString">Specifies the database connection string</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function UpdateTableQuery(ByVal connectionString As String, ByVal query As String) As Integer

    ''' <summary>
    '''  Executes a table update query.
    ''' </summary>
    ''' <param name="query">Specifies the query lines.</param>
    ''' <param name="connectionString">Specifies the database connection string</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function UpdateTableQuery(ByVal connectionString As String, ByVal query As String()) As Integer

#End Region

#Region " VERIFICATION "

    ''' <summary>
    ''' Gets the current version. Reads from the database if the stored version is null or faile reading the version or last
    ''' verification failed.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property CurrentReleaseVersion() As String

    ''' <summary>
    ''' Reads the current version.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function CurrentReleaseVersionGetter(ByVal connectionString As String) As String

    ''' <summary>
    ''' Gets or sets the text used to query the current release. 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property CurrentReleaseVersionQueryText() As String

    ''' <summary>
    ''' Returns true if the database was successfuly verified.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property DatabaseVerified() As Boolean

    ''' <summary>
    ''' Verifies that the local database is connectible and is current.
    ''' </summary>
    ''' <param name="connectionString">Specifies the database connection string</param>
    ''' <param name="version">Specifies the database version</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function VerifyDatabaseVersion(ByVal connectionString As String, ByVal version As String) As Boolean

#End Region

End Interface

