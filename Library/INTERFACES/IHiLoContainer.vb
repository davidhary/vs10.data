﻿''' <summary>
''' The general contract implemented by the class that contains the Hi/Lo information for
''' generating the Hi/Lo identifier.
''' </summary>
''' <remarks>
''' <para>
''' Implementors should provide a public default constructor.
''' </para>
''' <para>
''' Implementors <b>must</b> be threadsafe.
''' </para>
''' </remarks>
Public Interface IHiLoContainer

  ''' <summary>
  ''' Gets or sets a lock for fetching or saving to the identity store. 
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Property Lock() As Object

  ''' <summary>
  ''' Fetches the <see cref="HiLoRecord">identifier record</see> from the container.
  ''' </summary>
  ''' <returns>True if success</returns>
  ''' <remarks></remarks>
  Function FetchRecord() As HiLoRecord

  ''' <summary>
  ''' Saves the <see cref="HiLoRecord">identifier record</see> to the container.
  ''' </summary>
  ''' <param name="value"></param>
  ''' <returns>True if success</returns>
  ''' <remarks></remarks>
  Function SaveRecord(ByVal value As HiLoRecord) As Boolean

End Interface
