﻿''' <summary>
''' The general contract between a class that generates unique
''' identifiers and the <see cref="IHiLoContainer"/>.
''' </summary>
''' <remarks>
''' <para>
''' Implementors should provide a public default constructor.
''' </para>
''' <para>
''' Implementors <b>must</b> be threadsafe.
''' </para>
''' </remarks>
Public Interface IHiLoGenerator

  ''' <summary>
  ''' Generates and returns a new identifier.
  ''' </summary>
  ''' <param name="container"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Function Generate(ByVal container As IHiLoContainer) As Long

End Interface
