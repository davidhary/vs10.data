﻿''' <summary>
''' An <see cref="IHiLoContainer" /> that returns an <c>Int64</c>, constructed using
''' a hi/lo algorithm.
''' </summary>
''' <remarks>
''' Uses a database table to store a set of values for generating a long identifier constructed using a hi/lo algorithm.
''' The high value must be fetched in a separate transaction so the generator must be able to obtain a new connection
''' and commit it.
''' </remarks>
Public Class HiLoGenerator
    Implements IHiLoGenerator

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Private _hiLoRecord As HiLoRecord
    ''' <summary>
    ''' A constructor to initialize a hi/lo generator for generating identifiers
    ''' spanning the entire positive range of <see cref="Int32">32 bit integers</see>.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
        MyBase.New()
        _hiLoRecord = New HiLoRecord(0, 0, 0)
    End Sub

#End Region

#Region " I IDENTITY GENERATOR "

    ''' <summary>
    ''' Generates a new identifier. Saves the current record in the container. 
    ''' </summary>
    ''' <param name="container"></param>
    ''' <returns>The id or<para>
    ''' 0 if failed in Neew Identifier</para><para>
    ''' -1 if failed fetching the record</para><para>
    ''' -2 if failed saving the record</para></returns>
    ''' <remarks>
    ''' A <see cref="HiLoRecord.Low"></see> locally incremented value is used with a 
    ''' <see cref="HiLoRecord.seed">seed</see> to generate a <see cref="HiLoRecord.Identifier"></see>long identifier. 
    ''' When the locally incremented value exceeds a <see cref="HiLoRecord.MaximumLow">maximum</see>, a new 
    ''' <see cref="HiLoRecord.High">high</see> value is fetched from the 
    ''' container. This high value is used to create a new seed. 
    ''' </remarks>
    Public Overridable Function Generate(ByVal container As IHiLoContainer) As Long Implements IHiLoGenerator.Generate

        If _hiLoRecord.MaximumLow = 0 Then

            SyncLock container.Lock()
                _hiLoRecord = container.FetchRecord()
                If _hiLoRecord.High = 0 Then
                    Return -1
                End If
                Dim id As Long = _hiLoRecord.NewIdentifier()
                If id > 0 Then
                    If Not container.SaveRecord(_hiLoRecord) Then
                        id = -2
                    End If
                End If
                Return id
            End SyncLock

        ElseIf _hiLoRecord.Low > _hiLoRecord.MaximumLow Then

            SyncLock container.Lock()
                Dim record As HiLoRecord = container.FetchRecord()
                If record.High = 0 Then
                    Return -1
                End If
                _hiLoRecord = New HiLoRecord(record.High + 1, 1, record.MaximumLow)
                If Not container.SaveRecord(record) Then
                    Return -2
                End If
            End SyncLock

        Else
            SyncLock container.Lock()
                _hiLoRecord.NewIdentifier()
                If Not container.SaveRecord(_hiLoRecord) Then
                    Return -2
                End If
            End SyncLock
        End If

        Return _hiLoRecord.Identifier

    End Function

#End Region

End Class

''' <summary>
''' Holds the hi/lo record.
''' </summary>
''' <remarks>
''' </remarks>
Public Structure HiLoRecord

  Public Sub New(ByVal high As Int32, ByVal low As Int32, ByVal maximumLow As Int32)
    _high = high
    _low = low
    _maximumLow = maximumLow
  End Sub

  Public Sub New(ByVal record As HiLoRecord)
    Me.New(record.High, record.Low, record.MaximumLow)
  End Sub

#Region " EQUALS "

  ''' <summary>
  ''' Returns true if the intances are equal.
  ''' </summary>
  ''' <param name="left">Specifies a left hand value for comparison</param>
  ''' <param name="right">Specifies a righ hand value for comparison</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overloads Shared Function Equals(ByVal left As HiLoRecord, ByVal right As HiLoRecord) As Boolean
    Return left.High = right.High AndAlso _
           left.Low = right.Low AndAlso _
           left.MaximumLow = right.MaximumLow
  End Function

  ''' <summary>
  ''' Returns true if the intances are equal.
  ''' </summary>
  ''' <param name="left">Specifies a left hand value for comparison</param>
  ''' <param name="right">Specifies a righ hand value for comparison</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Operator =(ByVal left As HiLoRecord, ByVal right As HiLoRecord) As Boolean
    Return HiLoGenerator.Equals(left, right)
  End Operator

  ''' <summary>
  ''' Returns true if the intances are equal.
  ''' </summary>
  ''' <param name="left">Specifies a left hand value for comparison</param>
  ''' <param name="right">Specifies a righ hand value for comparison</param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Operator <>(ByVal left As HiLoRecord, ByVal right As HiLoRecord) As Boolean
    Return Not HiLoGenerator.Equals(left, right)
  End Operator

  ''' <summary>
  ''' Returns true if the values are equal
  ''' </summary>
  ''' <param name="value"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overloads Function Equals(ByVal value As HiLoRecord) As Boolean
    Return HiLoRecord.Equals(Me, value)
  End Function

  ''' <summary>
  ''' Returns true if the values are equal
  ''' </summary>
  ''' <param name="obj"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function Equals(ByVal obj As Object) As Boolean
    If obj Is Nothing OrElse Not TypeOf (obj) Is HiLoRecord Then
      Return False
    Else
      Return Me.Equals(CType(obj, HiLoRecord))
    End If
  End Function


  ''' <summary>
  ''' Returns a unique hash code for this instance.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function GetHashCode() As Integer
    Return Me._high.GetHashCode() Xor Me._low.GetHashCode() Xor Me._maximumLow.GetHashCode()
  End Function

#End Region

  Private _maximumLow As Int32
  ''' <summary>
  ''' Gets or sets the maximum value of the <see cref="_low">locally incremented value.</see>  
  ''' Once the locally incremented value exceeds the <see cref="_maximumLow">maximum</see> 
  ''' a new <see cref="_high">high value </see> is fetched from the <see cref="IHiLoContainer">container</see> 
  ''' for creating a new seed for generating a new sequence of identifiers. 
  ''' </summary>
  ''' <remarks></remarks>
  Public Property MaximumLow() As Int32
    Get
      Return _maximumLow
    End Get
    Set(ByVal value As Int32)
      _maximumLow = value
    End Set
  End Property

  Private _high As Int32
  ''' <summary>
  ''' Gets or sets the high value.
  ''' </summary>
  ''' <remarks></remarks>
  Public Property High() As Int32
    Get
      Return _high
    End Get
    Set(ByVal value As Int32)
      _high = value
    End Set
  End Property

  Private _low As Int32
  ''' <summary>
  ''' Gets or sets the locally incremented value.
  ''' </summary>
  ''' <remarks></remarks>
  Public Property Low() As Int32
    Get
      Return _low
    End Get
    Set(ByVal value As Int32)
      _low = value
    End Set
  End Property

  ''' <summary>
  ''' Gets the current identifier.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public ReadOnly Property Identifier() As Long
    Get
      Return Me.Seed + Me._low
    End Get
  End Property

  ''' <summary>
  ''' Increments the low value and returns a new identifier. 
  ''' </summary>
  ''' <remarks></remarks>
  Public Function NewIdentifier() As Long
    _low += 1
    Return Identifier
  End Function

  ''' <summary>
  ''' Gets the current seed.
  ''' </summary>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public ReadOnly Property Seed() As Long
    Get
      Return _maximumLow * (_high - 1)
    End Get
  End Property

  ''' <summary>
  ''' Gets the represeted value
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overrides Function ToString() As String
    Return String.Format(Globalization.CultureInfo.CurrentCulture, _
                         "Hi:{0},Lo:{1},MaxLo:{2}", _high, _low, _maximumLow)
  End Function

End Structure