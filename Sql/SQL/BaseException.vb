''' <summary>
''' An inheritable exception for use by ISR framework classes and applications.
''' </summary>
''' <remarks>
''' Inherits from System.Exception per FxCop design rule CA1958 which specifies 
''' that "Types do not extend inheritance vulnerable types" and further explains 
''' that "This [Applicaiton Exception] base exception type does not provide any additional value for 
''' framework classes." 
''' </remarks>
''' <license>
''' (c) 2007 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="01/24/07" by="David Hary" revision="1.0.2580.x">
''' created.
''' </history>
<Serializable()> Public Class BaseException
    Inherits System.Exception

#Region " CONSTRUCTORS "

    ''' <summary>
    ''' A parameterless constructor.
    ''' </summary>
    Public Sub New()
        MyBase.New()
        obtainEnvironmentInformation()
    End Sub

    ''' <summary>
    ''' Constructs the class specifying a <paramref name="message">message</paramref>.
    ''' </summary>
    ''' <param name="message">
    ''' Specifies the exception message.
    ''' </param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
        obtainEnvironmentInformation()
    End Sub

    ''' <summary>Constructor specifying the Message and InnerException property to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Sets a reference to the InnerException.</param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        Me.New(innerException, message)
    End Sub

    ''' <summary>Constructor specifying the Message and InnerException property to be set.</summary>
    ''' <param name="innerException">Sets a reference to the InnerException.</param>
    ''' <param name="format">Specifies the exception message format.</param>
    ''' <param name="args">Specifies the exception message arguments.</param>
    Public Sub New(ByVal innerException As System.Exception, ByVal format As String, ByVal ParamArray args() As Object)
        MyBase.New(String.Format(Globalization.CultureInfo.CurrentCulture, format, args), innerException)
        obtainEnvironmentInformation()
    End Sub

    ''' <summary>
    ''' Constructs the class using serialization <paramref name="info"/> and <paramref name="context"/>
    '''  information.
    ''' </summary>
    ''' <param name="info">
    ''' Specifies <see cref="Runtime.Serialization.SerializationInfo">serialization information</see>.
    ''' </param>
    ''' <param name="context">
    ''' Sepecifies <see cref="Runtime.Serialization.StreamingContext">streaming context</see> for the exception.
    ''' </param>
    Protected Sub New(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
        If info Is Nothing Then
            Return
        End If
        _additionalInformation = CType(info.GetValue("additionalInformation", _
            GetType(System.Collections.Specialized.NameValueCollection)), System.Collections.Specialized.NameValueCollection)
    End Sub

#End Region

#Region " METHODS AND PROPERTIES "

    ''' <summary>
    ''' Specifies the contents of the additional information.
    ''' </summary>
    ''' <remarks></remarks>
    Private Enum AdditionalInfoItem
        None
        MachineName
        Timestamp
        FullName
        AppDomainName
        ThreadIdentity
        WindowsIdentity
        OSVersion
    End Enum

    ''' <summary>
    ''' Overrides the <see cref="GetObjectData"/> method to serialize custom values.
    ''' </summary>
    ''' <param name="info">
    ''' Specifies <see cref="Runtime.Serialization.SerializationInfo">serialization information</see>.
    ''' </param>
    ''' <param name="context">
    ''' Sepecifies <see cref="Runtime.Serialization.StreamingContext">streaming context</see> for the exception.
    ''' </param>
    <Security.Permissions.SecurityPermission(Security.Permissions.SecurityAction.Demand, SerializationFormatter:=True), _
      Security.Permissions.SecurityPermission(Security.Permissions.SecurityAction.LinkDemand, Flags:=Security.Permissions.SecurityPermissionFlag.SerializationFormatter)> _
    Public Overrides Sub GetObjectData(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)

        If info Is Nothing Then
            Return
        End If
        info.AddValue("AdditionalInformation", _additionalInformation, GetType(System.Collections.Specialized.NameValueCollection))
        MyBase.GetObjectData(info, context)

    End Sub

    ''' <summary>
    ''' Gathers environment information safely.
    ''' </summary>
    Private Sub obtainEnvironmentInformation()

        _additionalInformation = New System.Collections.Specialized.NameValueCollection
        _additionalInformation.Add(AdditionalInfoItem.MachineName.ToString, My.Computer.Name)
        _additionalInformation.Add(AdditionalInfoItem.Timestamp.ToString, Date.Now.ToString(System.Globalization.CultureInfo.CurrentCulture))
        _additionalInformation.Add(AdditionalInfoItem.FullName.ToString, System.Reflection.Assembly.GetExecutingAssembly().FullName)
        _additionalInformation.Add(AdditionalInfoItem.AppDomainName.ToString, AppDomain.CurrentDomain.FriendlyName)
        _additionalInformation.Add(AdditionalInfoItem.ThreadIdentity.ToString, My.User.Name) ' Thread.CurrentPrincipal.Identity.Name
        _additionalInformation.Add(AdditionalInfoItem.WindowsIdentity.ToString, My.Computer.Info.OSFullName)
        _additionalInformation.Add(AdditionalInfoItem.OSVersion.ToString, My.Computer.Info.OSVersion)

    End Sub

    Private _additionalInformation As New System.Collections.Specialized.NameValueCollection
    ''' <summary>
    ''' Collection allowing additional information to be added to the exception.
    ''' </summary>
    Public ReadOnly Property AdditionalInformation() As System.Collections.Specialized.NameValueCollection
        Get
            Return _additionalInformation
        End Get
    End Property

#End Region

End Class

