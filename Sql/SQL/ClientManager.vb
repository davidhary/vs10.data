Namespace Sql

    ''' <summary>Includes support functionality for SQL server.</summary>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="11/07/06" by="David Hary" revision="1.0.2502.x">
    ''' created.
    ''' </history>
    ''' <history date="11/07/06" by="David Hary" revision="1.0.2502.x">
    ''' Source: http://www.codeproject.com/useritems/Deploy_your_database.asp.
    ''' Converted to .NET.
    ''' </history>
    Public NotInheritable Class ClientManager

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>
        ''' Construtor for this class.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()
            Me.New("SQLEXPRESS")
        End Sub

        ''' <summary>
        ''' Construtor for this class.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New(ByVal localInstanceName As String)
            MyBase.New()
            _defaultBackupFileExtension = ".bak"
            Me.InstanceName = localInstanceName
        End Sub

#End Region

#Region " CONNECTIONS "

        ''' <summary>
        ''' Returns a connection string to an SQL catalog on the specified local server instance.
        ''' </summary>
        ''' <param name="pattern">Specifies the connection string pattern.</param>
        ''' <param name="instanceName">Specifies the local server instance name.</param>
        ''' <param name="databaseName">Specifies the database name.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function BuildLocalConnectionString(ByVal pattern As String, _
            ByVal instanceName As String, ByVal databaseName As String) As String

            If String.IsNullOrEmpty(pattern) Then
                Throw New ArgumentNullException("pattern")
            End If

            If String.IsNullOrEmpty(databaseName) Then
                Throw New ArgumentNullException("databaseName")
            End If

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            Return String.Format(Globalization.CultureInfo.CurrentCulture, _
                  pattern, ClientManager.BuildLocalServerInstanceName(instanceName), databaseName)

        End Function

        ''' <summary>
        ''' Returns a connection string to an SQL catalog on the specified local server instance.
        ''' </summary>
        ''' <param name="instanceName">Specifies the local server instance name.</param>
        ''' <param name="databaseName">Specifies the database name.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function BuildLocalConnectionStringLlblgen(ByVal instanceName As String, ByVal databaseName As String) As String

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            If String.IsNullOrEmpty(databaseName) Then
                Throw New ArgumentNullException("databaseName")
            End If

            Return ClientManager.BuildLocalConnectionString(_llblgenConnectionStringPattern, instanceName, databaseName)

        End Function

        ''' <summary>
        ''' Returns a connection string to an SQL catalog on the specified local server instance.
        ''' </summary>
        ''' <param name="instanceName">Specifies the local server instance name.</param>
        ''' <param name="databaseName">Specifies the database name.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function BuildLocalConnectionString(ByVal instanceName As String, ByVal databaseName As String) As String

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            If String.IsNullOrEmpty(databaseName) Then
                Throw New ArgumentNullException("databaseName")
            End If

            Return ClientManager.BuildLocalConnectionString(_connectionStringPattern, instanceName, databaseName)

        End Function

        ''' <summary>
        ''' Connects to the specified database on the specified local server instance.
        ''' </summary>
        ''' <param name="instanceName">Specifies the server instance name, such as 'SQLEXPRESS'</param>
        ''' <param name="databaseName"></param>
        ''' <remarks></remarks>
        Public Shared Function ConnectLocalDatabase(ByVal instanceName As String, _
            ByVal databaseName As String) As System.Data.SqlClient.SqlConnection

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            If String.IsNullOrEmpty(databaseName) Then
                Throw New ArgumentNullException("databaseName")
            End If

            ' check if we have a local SQL server.
            Dim server As New Microsoft.SqlServer.Management.Smo.Server(System.Environment.MachineName)
            If server Is Nothing Then
                Throw New isr.Data.Sql.LocalServerNotFoundException()
            End If

            Dim conn As System.Data.SqlClient.SqlConnection = Nothing

            ' Get the connection string to the master catalog
            Dim connString As String = ClientManager.BuildLocalConnectionString(instanceName, databaseName)

            My.Application.Log.WriteEntry("Created connection string to local database: ", TraceEventType.Verbose)
            My.Application.Log.WriteEntry(connString, TraceEventType.Verbose)
            conn = New System.Data.SqlClient.SqlConnection(connString)
            If conn.State <> System.Data.ConnectionState.Open Then
                My.Application.Log.WriteEntry("Connecting to database", TraceEventType.Verbose)
                conn.Open()
            End If
            My.Application.Log.WriteEntry("Connected to database", TraceEventType.Verbose)

            Return conn

        End Function

        Private Shared _connectionStringPattern As String = "data source={0};initial catalog={1};integrated security=SSPI;persist security info=False"
        ''' <summary>
        ''' Gets or set the default connection string format pattern.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Property ConnectionStringPattern() As String
            Get
                Return _connectionStringPattern
            End Get
            Set(ByVal value As String)
                _connectionStringPattern = value
            End Set
        End Property

        ''' <summary>
        ''' Gets the conditions for telling if any sql server exists on the local machine.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared ReadOnly Property IsAnyLocalServerExists() As Boolean
            Get
                Dim server As New Microsoft.SqlServer.Management.Smo.Server(System.Environment.MachineName)
                Return server IsNot Nothing
            End Get
        End Property

        Private Shared _llblgenConnectionStringPattern As String = "data source={0};initial catalog={1};integrated security=SSPI;persist security info=False;packet size=4096"
        ''' <summary>
        ''' Gets or set the LLBLGEN connection string format pattern.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Property LlblgenConnectionStringPattern() As String
            Get
                Return _llblgenConnectionStringPattern
            End Get
            Set(ByVal value As String)
                _llblgenConnectionStringPattern = value
            End Set
        End Property

        ''' <summary>
        ''' Returns true if able to connect to the database using the specified connection string.
        ''' </summary>
        ''' <param name="connectionString">Specifies the connection string to connect to.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function TryConnect(ByVal connectionString As String) As Boolean

            If String.IsNullOrEmpty(connectionString) Then
                Throw New ArgumentNullException("connectionString")
            End If

            ' connect to the local database.
            Using conn As New System.Data.SqlClient.SqlConnection(connectionString)
                Try
                    conn.Open()
                    Return conn.State = ConnectionState.Open
                Catch ex As System.Data.SqlClient.SqlException
                    Throw New BaseException(ex, "Failed connecting to the database using '{0}'", connectionString)
                Finally
                    If conn.State = ConnectionState.Open Then
                        conn.Close()
                    End If
                End Try
            End Using
            Return False

        End Function

        ''' <summary>
        ''' Write the local connection string to the configuration file.
        ''' </summary>
        ''' <param name="instanceName">Specifies the local server instance name, such as 'SQLEXPRESS'</param>
        ''' <param name="configFilePathName">Specifies the configuration file name.</param>
        ''' <remarks></remarks>
        Public Shared Sub WriteLocalConnectionString(ByVal instanceName As String, _
            ByVal databaseName As String, ByVal configFilePathName As String)

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            If String.IsNullOrEmpty(databaseName) Then
                Throw New ArgumentNullException("databaseName")
            End If

            If String.IsNullOrEmpty(configFilePathName) Then
                Throw New ArgumentNullException("configFilePathName")
            End If

            ' Create connection string to datbase
            Dim connString As String = ClientManager.BuildLocalConnectionString(instanceName, databaseName)
            My.Application.Log.WriteEntry("Built Connection String:")
            My.Application.Log.WriteEntry(connString)

            ' Updating connection string in file 
            My.Application.Log.WriteEntry("Saving connection string to:")
            My.Application.Log.WriteEntry(configFilePathName)

            Dim xmlDom As System.Xml.XmlDocument = New System.Xml.XmlDocument()
            xmlDom.Load(configFilePathName)

            ' Get XML node 
            Dim xmlNode As System.Xml.XmlNode = xmlDom.SelectSingleNode("configuration/appSettings/add[@key='ConnString']")
            xmlNode.Attributes("value").Value = connString

            ' Updating connection string in file 
            My.Application.Log.WriteEntry("Updating node of config file:")
            My.Application.Log.WriteEntry(xmlNode.InnerXml)

            ' Save to disk
            xmlDom.Save(configFilePathName)

        End Sub

#End Region

#Region " SERVER "

        ''' <summary>
        ''' Returns a server name in the format 'Machine Name' or'Machine Name\Instance name', e.g.,
        ''' "ABC\SQLEXPRESS".
        ''' </summary>
        ''' <param name="instanceName">Specifies the local server instance name.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function BuildLocalServerInstanceName(ByVal instanceName As String) As String

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            Dim dataSourcePattern As String = "{0}\{1}"

            If String.IsNullOrEmpty(instanceName) Then
                Return "localhost" '  Environment.MachineName
            Else
                Return String.Format(Globalization.CultureInfo.CurrentCulture, _
                        dataSourcePattern, Environment.MachineName, instanceName)
            End If

        End Function

        ''' <summary>
        ''' Disconnects the server.
        ''' </summary>
        ''' <param name="server"></param>
        ''' <remarks></remarks>
        Public Shared Sub DisconnectServer(ByVal server As Microsoft.SqlServer.Management.Smo.Server)

            If server IsNot Nothing AndAlso Not String.IsNullOrEmpty(server.ConnectionContext.ConnectionString) Then
                Dim conn As New System.Data.SqlClient.SqlConnection(server.ConnectionContext.ConnectionString)
                Using conn
                    ' the connection state does not reflect the state of the server
                    If conn.State = System.Data.ConnectionState.Open Then
                        conn.Close()
                    Else
                        conn.Close()
                    End If
                End Using
            End If

        End Sub

        ''' <summary>
        ''' Connects to the local server instance and returns an instance of this server object.
        ''' </summary>
        ''' <param name="instanceName">Specifies the instance name to connect to.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function ConnectLocalServer(ByVal instanceName As String) As Microsoft.SqlServer.Management.Smo.Server

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            Dim conn As System.Data.SqlClient.SqlConnection = Nothing
            ' connect to the master database.
            conn = ClientManager.ConnectLocalDatabase(instanceName, "master")

            Dim svrConnection As New Microsoft.SqlServer.Management.Common.ServerConnection(conn)
            Dim svr As New Microsoft.SqlServer.Management.Smo.Server(svrConnection)
            Return svr

        End Function

        Private Shared _locatedServerName As String
        ''' <summary>
        ''' Gets the locateed server name.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared ReadOnly Property LocatedServerName() As String
            Get
                Return _locatedServerName
            End Get
        End Property

        ''' <summary>
        ''' Gets the server identity name format in the form
        ''' 'SQLServer2005MSSQLUser$ComputerName$InstanceName'
        ''' </summary>
        ''' <remarks></remarks>
        Private Shared identityNameFormat As String = "SQLServer2005MSSQLUser$%1$%2"
        ''' <summary>
        ''' Gets the identity used for setting access rights to the SQL server.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared ReadOnly Property LocalServerIdentifyName() As String
            Get
                Dim identityName As String = identityNameFormat
                identityName = identityName.Replace("%1", System.Environment.MachineName)
                identityName = identityName.Replace("%2", "SQLEXPRESS")
                Return identityName
            End Get
        End Property

        ''' <summary>
        ''' Returns the data source table of: Server Name, Instance Name, Is Clustered, and Version.  
        ''' SQL 2005 is version 9.0.x.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function EnumerateServers() As System.Data.DataTable

            Dim sqlEnum As System.Data.Sql.SqlDataSourceEnumerator = System.Data.Sql.SqlDataSourceEnumerator.Instance

            ' get the data source table of: Server Name, Instance Name, Is Clustered, and Version.  SQL 2005 is 
            ' version 9.0.x.
            Return sqlEnum.GetDataSources()

        End Function

        ''' <summary>
        ''' Fetches the local server role name.
        ''' </summary>
        ''' <param name="instanceName">Specifies the local server instance name, such as 'SQLEXPRESS'</param>
        ''' <returns>The path name for SQL data..</returns>
        ''' <remarks></remarks>
        Public Shared Function FetchLocalServerRoleName(ByVal instanceName As String) As String

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            Dim conn As System.Data.SqlClient.SqlConnection = Nothing
            Try

                ' connect to the master database.
                conn = ClientManager.ConnectLocalDatabase(instanceName, "master")
                Dim svrConnection As New Microsoft.SqlServer.Management.Common.ServerConnection(conn)
                Dim svr As New Microsoft.SqlServer.Management.Smo.Server(svrConnection)
                Dim rollName As String = svr.Roles(0).Name
                Return rollName

            Finally

                If Not conn Is Nothing Then
                    conn.Dispose()
                End If

            End Try

        End Function

#End Region

#Region " BACKUP "

        Private _defaultBackupFileExtension As String
        ''' <summary>
        ''' Gets or sets the default backup file extension.
        ''' Updates the file info if a connection string is defined.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property DefaultBackupFileExtension() As String
            Get
                Return _defaultBackupFileExtension
            End Get
            Set(ByVal value As String)
                _defaultBackupFileExtension = value
            End Set
        End Property

        ''' <summary>
        ''' Backup the specified database on the local server instance.  A differential backup is 
        ''' done if the backup already exists.
        ''' </summary>
        ''' <param name="instanceName">Specifies the local server instance name, such as 'SQLEXPRESS'</param>
        ''' <param name="databaseName">Specifies the database name.</param>
        ''' <param name="backupFilePathName">Specifies the file name path of the database backup.</param>
        ''' <param name="backupName">Specifies the backup name.</param>
        ''' <returns>The backup file path name for the database file.</returns>
        ''' <remarks></remarks>
        Public Shared Function BackupLocalDatabase(ByVal instanceName As String, _
            ByVal databaseName As String, _
            ByVal backupFilePathName As String, ByVal backupName As String) As String

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            If String.IsNullOrEmpty(databaseName) Then
                Throw New ArgumentNullException("databaseName")
            End If

            If String.IsNullOrEmpty(backupFilePathName) Then
                Throw New ArgumentNullException("backupFilePathName")
            End If

            If String.IsNullOrEmpty(backupName) Then
                Throw New ArgumentNullException("backupName")
            End If

            Dim lastAction As String
            Dim cmd As System.Data.SqlClient.SqlCommand = Nothing
            Dim conn As System.Data.SqlClient.SqlConnection = Nothing

            Try

                ' connect to the master database.
                conn = ClientManager.ConnectLocalDatabase(instanceName, "master")

                Const fullBackupSyntax As String = "BACKUP DATABASE [{0}] TO DISK = '{1}' WITH FORMAT, NAME = '{2}'"
                Const diffBackupSyntax As String = "BACKUP DATABASE [{0}] TO DISK = '{1}' WITH DIFFERENTIAL"

                Dim isDifferential As Boolean = System.IO.File.Exists(backupFilePathName)
                Dim query As String
                ' check if database file exists
                If isDifferential Then
                    lastAction = "Differential "
                    query = String.Format(Globalization.CultureInfo.CurrentCulture, _
                        diffBackupSyntax, databaseName, backupFilePathName)
                Else
                    lastAction = "Full "
                    query = String.Format(Globalization.CultureInfo.CurrentCulture, _
                        fullBackupSyntax, databaseName, backupFilePathName, backupName)
                End If
                lastAction = lastAction & "backup of database: [{0}] & to '{1}'."
                lastAction = String.Format(Globalization.CultureInfo.CurrentCulture, lastAction, databaseName, backupFilePathName)

                lastAction = "Created query on the local SQL server:"
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
                My.Application.Log.WriteEntry(query, TraceEventType.Verbose)

                lastAction = "Creating command on the local SQL server..."
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)

                ' Create SQL query 
                cmd = New System.Data.SqlClient.SqlCommand(query, conn)
                If cmd.Connection.State <> ConnectionState.Open Then
                    cmd.Connection.Open()
                End If
                cmd.CommandType = System.Data.CommandType.Text

                ' return the main path name
                BackupLocalDatabase = backupFilePathName

                lastAction = "Created command to execute on the local SQL server:"
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
                My.Application.Log.WriteEntry(cmd.CommandText, TraceEventType.Verbose)

                lastAction = "Executing command on the local SQL server..."
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
                cmd.ExecuteNonQuery()

                My.Application.Log.WriteEntry("Done database backup.", TraceEventType.Verbose)

            Finally

                If Not cmd Is Nothing Then
                    cmd.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If

            End Try

        End Function

        ''' <summary>
        ''' Gets the backup folder path name.
        ''' Gets a time stamped backup folder path name, e.g., '..\MSQL\Backup'.
        ''' </summary>
        ''' <param name="instanceName">Specifies the local server instance name, such as 'SQLEXPRESS'</param>
        ''' <returns>The backup file path name.</returns>
        ''' <remarks></remarks>
        Public Shared Function BuildLocalBackupFolderPathName(ByVal instanceName As String) As String

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            Dim pathName As String
            pathName = System.IO.Path.Combine(ClientManager.FetchDataParentPathName(instanceName), "\Backup")
            Return pathName

        End Function

        ''' <summary>
        ''' Time stamps the current backup.  
        ''' </summary>
        ''' <remarks>Prior to calling this method, make sure to run a final differential
        ''' backup.</remarks>
        Public Function ArchiveBackup() As Boolean

            Dim lastAction As String

            Me._lastTimeStampedBackupFilePathName = Me.TimestampBackupFilePathName
            lastAction = "Renaming '" & Me.BackupFilePathName & "' to '" & _lastTimeStampedBackupFilePathName & "'."
            OnMessageAvailable(lastAction, False, lastAction)
            My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
            System.IO.File.Move(Me.BackupFilePathName, _lastTimeStampedBackupFilePathName)
            Return True

        End Function

        ''' <summary>
        ''' Backs up the database.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function BackupDatabase() As Boolean

            'Me.LocateServerAndDatabase()

            Dim lastAction As String

            lastAction = "Backing up '" & _databaseName & "' on '" & Me.InstanceName & "'."
            OnMessageAvailable(lastAction, False, lastAction)
            My.Application.Log.WriteEntry("Dropped " & lastAction, TraceEventType.Verbose)

            ' backup and get the backup file name.
            lastAction = isr.Data.Sql.ClientManager.BackupLocalDatabase(Me.InstanceName, _databaseName, Me.BackupFilePathName, _backupName)

            If lastAction.Contains("\") Then
                lastAction = "Database file backed up to '" & lastAction & "'"
                OnMessageAvailable(lastAction, False, lastAction)
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
                BackupDatabase = True
            Else
                BackupDatabase = False
                lastAction = "Failed backing up database. " & lastAction
                OnMessageAvailable(lastAction, False, lastAction)
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
            End If

        End Function

        ''' <summary>
        ''' Return a time stamped file name for archiving the backup file.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function BuildTimeStampedBackupFileName() As String
            Return _databaseName & _
                String.Format(Globalization.CultureInfo.CurrentCulture, _
                    _backupFileSuffix & _timestampFormat & _defaultBackupFileExtension, Now)
        End Function

#End Region

#Region " DATABASE "

        ''' <summary>
        ''' Deattaches the database from the SQL server.
        ''' </summary>
        ''' <param name="instanceName"></param>
        ''' <param name="catalogName"></param>
        ''' <remarks></remarks>
        Public Shared Sub DetachLocalDatabase(ByVal instanceName As String, _
          ByVal catalogName As String)

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            If String.IsNullOrEmpty(catalogName) Then
                Throw New ArgumentNullException("catalogName")
            End If

            Dim svr As Microsoft.SqlServer.Management.Smo.Server = ConnectLocalServer(instanceName)
            svr.DetachDatabase(catalogName, True)

        End Sub

        ''' <summary>
        ''' Removes the database from the SQL server and returns a comma separated list
        ''' of the database and log file names.
        ''' </summary>
        ''' <param name="instanceName"></param>
        ''' <param name="catalogName"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function DropLocalDatabase(ByVal instanceName As String, _
          ByVal catalogName As String) As String

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            If String.IsNullOrEmpty(catalogName) Then
                Throw New ArgumentNullException("catalogName")
            End If

            Dim databases As Microsoft.SqlServer.Management.Smo.DatabaseCollection = ClientManager.EnumerateLocalDatabases(instanceName)

            If databases Is Nothing OrElse databases.Count <= 0 Then
                My.Application.Log.WriteEntry("Databases not found.", TraceEventType.Critical)
            End If

            Dim database As Microsoft.SqlServer.Management.Smo.Database
            If databases.Contains(catalogName) Then

                database = databases.Item(catalogName)
                DropLocalDatabase = database.FileGroups(0).Files(0).FileName & "," _
                    & database.LogFiles(0).FileName
                database.Drop()

            Else

                My.Application.Log.WriteEntry("Failed dropping database because the database does not exist.  System reports the following databases:", TraceEventType.Critical)
                For Each database In databases
                    My.Application.Log.WriteEntry(database.Name, TraceEventType.Critical)
                Next
                Return "Database not found"

            End If

        End Function

        ''' <summary>
        ''' Returns the collection of databases in the specified server.
        ''' </summary>
        ''' <param name="connectionString">Specifies the connection string to connect to.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function EnumerateDatabases(ByVal connectionString As String) As Microsoft.SqlServer.Management.Smo.DatabaseCollection

            If String.IsNullOrEmpty(connectionString) Then
                Throw New ArgumentNullException("connectionString")
            End If

            Using conn As New System.Data.SqlClient.SqlConnection(connectionString)

                ' connect to the master database.
                conn.ConnectionString = conn.ConnectionString.Replace(conn.Database, "master")

                Dim svrConnection As New Microsoft.SqlServer.Management.Common.ServerConnection(conn)
                Dim svr As New Microsoft.SqlServer.Management.Smo.Server(svrConnection)
                Return svr.Databases

            End Using

        End Function

        ''' <summary>
        ''' Returns the collection of databases in the specified server.
        ''' </summary>
        ''' <param name="instanceName">Specifies the instance name to connect to.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function EnumerateLocalDatabases(ByVal instanceName As String) As Microsoft.SqlServer.Management.Smo.DatabaseCollection

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            Dim conn As System.Data.SqlClient.SqlConnection = Nothing
            Try

                ' connect to the master database.
                conn = ClientManager.ConnectLocalDatabase(instanceName, "master")

                Dim svrConnection As New Microsoft.SqlServer.Management.Common.ServerConnection(conn)
                Dim svr As New Microsoft.SqlServer.Management.Smo.Server(svrConnection)
                Return svr.Databases

            Finally

                If Not conn Is Nothing Then
                    conn.Dispose()
                End If

            End Try

        End Function

        ''' <summary>
        ''' Fetches the data folder path name for the specified instance., e.g., 
        ''' 'C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Data'.
        ''' </summary>
        ''' <param name="instanceName">Specifies the local server instance name, such as 'SQLEXPRESS'</param>
        ''' <returns>The path name for SQL data..</returns>
        ''' <remarks></remarks>
        Public Shared Function FetchLocalMasterDatabasePathName(ByVal instanceName As String) As String

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            ' check if local server exists.
            Dim server As Microsoft.SqlServer.Management.Smo.Server
            server = isr.Data.Sql.ClientManager.ConnectLocalServer(instanceName)
            Dim pathName As String = server.Information.MasterDBPath
            Return pathName

        End Function

        ''' <summary>
        ''' Fetches the parent path of the data folder for the specified instance., e.g., 
        ''' 'C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL'.
        ''' </summary>
        ''' <param name="instanceName">Specifies the local server instance name, such as 'SQLEXPRESS'</param>
        ''' <returns>The path name for SQL data..</returns>
        ''' <remarks></remarks>
        Public Shared Function FetchDataParentPathName(ByVal instanceName As String) As String

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            ' get the path name
            Dim pathName As String = ClientManager.FetchLocalMasterDatabasePathName(instanceName)

            ' get the parent folder of the data folder.
            Dim di As New System.IO.DirectoryInfo(pathName)
            Return di.Parent.FullName

        End Function

        ''' <summary>
        ''' Returns information about the specified server.
        ''' </summary>
        ''' <param name="instanceName"></param>
        ''' <param name="catalogName"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function FetchLocalDatabaseInfo(ByVal instanceName As String, _
          ByVal catalogName As String) As String

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            If String.IsNullOrEmpty(catalogName) Then
                Throw New ArgumentNullException("catalogName")
            End If

            Dim databases As Microsoft.SqlServer.Management.Smo.DatabaseCollection = ClientManager.EnumerateLocalDatabases(instanceName)

            If databases Is Nothing OrElse databases.Count <= 0 Then
                My.Application.Log.WriteEntry("Databases not found.", TraceEventType.Critical)
                Return "Databases not found."
            End If

            Dim database As Microsoft.SqlServer.Management.Smo.Database
            If databases.Contains(catalogName) Then

                database = databases.Item(catalogName)
                Return FetchLocalDatabaseInfo(database)

            Else

                My.Application.Log.WriteEntry("Failed locating database.  System reports the following databases:", TraceEventType.Critical)
                For Each database In databases
                    My.Application.Log.WriteEntry(database.Name, TraceEventType.Critical)
                Next
                Return "Database not found."

            End If

        End Function

        ''' <summary>
        ''' Fetch information about the database.
        ''' </summary>
        ''' <param name="database"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function FetchLocalDatabaseInfo(ByVal database As Microsoft.SqlServer.Management.Smo.Database) As String

            Dim info As New System.Text.StringBuilder

            If database IsNot Nothing Then
                info.Append("Database Name = ")
                info.Append(database.Name)
                info.Append(Environment.NewLine)

                info.Append("Database Creasted = ")
                info.Append(database.CreateDate.ToLongDateString)
                info.Append(Environment.NewLine)

                info.Append("Default File Group = ")
                info.Append(database.DefaultFileGroup)
                info.Append(Environment.NewLine)

                info.Append("Default Full Text Catalog = ")
                info.Append(database.DefaultFullTextCatalog)
                info.Append(Environment.NewLine)

                info.Append("File Name = ")
                info.Append(database.FileGroups(0).Files(0).FileName)
                info.Append(Environment.NewLine)

                info.Append("Last Backup Date = ")
                info.Append(database.LastBackupDate.ToLongDateString)
                info.Append(Environment.NewLine)

                info.Append("Log File Name = ")
                info.Append(database.LogFiles(0).FileName)
                info.Append(Environment.NewLine)

                info.Append("Primary File Path = ")
                info.Append(database.PrimaryFilePath)
                info.Append(Environment.NewLine)

                info.Append("Size (MB) = ")
                info.Append(database.Size)
                info.Append(Environment.NewLine)

                info.Append("Space Available (KB) = ")
                info.Append(database.SpaceAvailable)
                info.Append(Environment.NewLine)

                info.Append("UserName = ")
                info.Append(database.UserName)
                info.Append(Environment.NewLine)
            End If

            Return info.ToString

        End Function

        ''' <summary>
        ''' Returns information about the specified server.
        ''' </summary>
        ''' <param name="instanceName"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function FetchLocalServerInfo(ByVal instanceName As String) As String

            Dim server As Microsoft.SqlServer.Management.Smo.Server = Nothing

            Try

                ' check if local server exists.
                server = isr.Data.Sql.ClientManager.ConnectLocalServer(instanceName)

                If server Is Nothing Then

                    Return "Local server not found."

                Else

                    Return isr.Data.Sql.ClientManager.FetchLocalServerInfo(server)

                End If

            Finally

                If server IsNot Nothing Then

                    isr.Data.Sql.ClientManager.DisconnectServer(server)

                End If

            End Try

        End Function

        ''' <summary>
        ''' Returns information about the specified server.
        ''' </summary>
        ''' <param name="server">Specifies the <see cref="Microsoft.SqlServer.Management.Smo.Server">server instance</see>.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function FetchLocalServerInfo(ByVal server As Microsoft.SqlServer.Management.Smo.Server) As String

            Dim info As New System.Text.StringBuilder

            If server IsNot Nothing Then

                ' SqlServerNameTextBox.Text = server.InstanceName
                info.Append("InstanceName = ")
                info.Append(server.InstanceName)
                info.Append(Environment.NewLine)
                info.Append("Name = ")
                info.Append(server.Name)
                info.Append(Environment.NewLine)

                'info.Append(Environment.NewLine)
                'info.Append("ApplicationName = ")
                'info.Append(server.ConnectionContext.ApplicationName.ToString)
                info.Append(Environment.NewLine)
                info.Append("ConnectionString = ")
                info.Append(server.ConnectionContext.ConnectionString)
                info.Append("Role Name = ")
                info.Append(server.Roles(0).Name)
                info.Append(Environment.NewLine)
                info.Append("DatabaseName = ")
                info.Append(server.ConnectionContext.DatabaseName)
                info.Append(Environment.NewLine)
                info.Append("ServerInstance = ")
                info.Append(server.ConnectionContext.ServerInstance)
                info.Append(Environment.NewLine)
                info.Append("ServerVersion = ")
                info.Append(server.ConnectionContext.ServerVersion)
                info.Append(Environment.NewLine)
                info.Append("WorkstationId = ")
                info.Append(server.ConnectionContext.WorkstationId)
                info.Append(Environment.NewLine)

                info.Append(Environment.NewLine)
                info.Append("Edition = ")
                info.Append(server.Information.Edition)
                info.Append(Environment.NewLine)
                info.Append("ErrorLogPath = ")
                info.Append(server.Information.ErrorLogPath)
                info.Append(Environment.NewLine)
                info.Append("MasterDBLogPath = ")
                info.Append(server.Information.MasterDBLogPath)
                info.Append(Environment.NewLine)
                info.Append("MasterDBPath = ")
                info.Append(server.Information.MasterDBPath)
                info.Append(Environment.NewLine)
                info.Append("NetName = ")
                info.Append(server.Information.NetName)
                info.Append(Environment.NewLine)
                info.Append("Platform = ")
                info.Append(server.Information.Platform)
                info.Append(Environment.NewLine)
                info.Append("Product = ")
                info.Append(server.Information.Product)
                info.Append(Environment.NewLine)
                info.Append("ProductLevel = ")
                info.Append(server.Information.ProductLevel)
                info.Append(Environment.NewLine)
                info.Append("RootDirectory = ")
                info.Append(server.Information.RootDirectory)
                'info.Append(Environment.NewLine)
                'info.Append("State = ")
                'info.Append(server.Information.State.ToString)
                info.Append(Environment.NewLine)
                info.Append("Version = ")
                info.Append(server.Information.VersionString)

            End If

            Return info.ToString

        End Function

        ''' <summary>
        ''' Returns Local Server Version string or the zero (00.0.00.00) version if server not found.
        ''' </summary>
        ''' <param name="instanceName"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function FetchLocalServerVersion(ByVal instanceName As String) As System.Version

            Dim server As Microsoft.SqlServer.Management.Smo.Server = Nothing

            Try

                ' check if local server exists.
                server = isr.Data.Sql.ClientManager.ConnectLocalServer(instanceName)

                If server Is Nothing Then

                    Return New System.Version("00.0.00.00")

                Else

                    Return server.Information.Version

                End If

            Finally

                If server IsNot Nothing Then

                    isr.Data.Sql.ClientManager.DisconnectServer(server)

                End If

            End Try

        End Function

        ''' <summary>
        ''' Gets a single record from the server.
        ''' </summary>
        ''' <param name="instanceName"></param>
        ''' <param name="databaseName"></param>
        ''' <param name="query"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function FetchRecord(ByVal instanceName As String, _
            ByVal databaseName As String, ByVal query As String) As System.Data.SqlClient.SqlDataReader

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            If String.IsNullOrEmpty(databaseName) Then
                Throw New ArgumentNullException("databaseName")
            End If

            If String.IsNullOrEmpty(query) Then
                Throw New ArgumentNullException("query")
            End If

            Dim lastAction As String
            Dim cmd As System.Data.SqlClient.SqlCommand = Nothing

            Try

                lastAction = "Creating command on the local SQL server..."
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)

                ' Create SQL query 
                cmd = New System.Data.SqlClient.SqlCommand(query)
                cmd.Connection = ClientManager.ConnectLocalDatabase(instanceName, databaseName)
                cmd.CommandType = System.Data.CommandType.Text

                lastAction = "Created command to execute on the local SQL server:"
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
                My.Application.Log.WriteEntry(cmd.CommandText, TraceEventType.Verbose)

                lastAction = "Executing command on the local SQL server..."
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
                Return cmd.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleRow)

            Finally

                If Not cmd Is Nothing Then
                    cmd.Dispose()
                    cmd = Nothing
                End If

            End Try

        End Function

        ''' <summary>
        ''' Returns true if a database exists on the local server instance.
        ''' </summary>
        ''' <param name="connectionString">Specifies the connection string.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function IsCatalogExists(ByVal connectionString As String) As Boolean

            If String.IsNullOrEmpty(connectionString) Then
                Throw New ArgumentNullException("connectionString")
            End If

            Dim affirmative As Boolean
            Using conn As New System.Data.SqlClient.SqlConnection(connectionString)

                Dim catalogName As String = conn.Database

                Dim databases As Microsoft.SqlServer.Management.Smo.DatabaseCollection = ClientManager.EnumerateDatabases(connectionString)

                If databases Is Nothing OrElse databases.Count <= 0 Then
                    My.Application.Log.WriteEntry("Databases not found.", TraceEventType.Critical)
                    Return False
                End If

                affirmative = databases.Contains(catalogName)

                If Not affirmative Then
                    ' if not located, report all the server that were found
                    My.Application.Log.WriteEntry("Failed locating database.  System reports the following databases:", TraceEventType.Critical)
                    For Each database As Microsoft.SqlServer.Management.Smo.Database In databases
                        My.Application.Log.WriteEntry(database.Name, TraceEventType.Critical)
                    Next
                End If

            End Using
            Return affirmative

        End Function

        ''' <summary>
        ''' Returns true if a database exists on the local server instance.
        ''' </summary>
        ''' <param name="instanceName">Specifies the server instance name.  Could be empty if the
        ''' default server.</param>
        ''' <param name="catalogName">Specifies the database name.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function IsLocalCatalogExists(ByVal instanceName As String, _
            ByVal catalogName As String) As Boolean

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            If String.IsNullOrEmpty(catalogName) Then
                Throw New ArgumentNullException("catalogName")
            End If

            Dim databases As Microsoft.SqlServer.Management.Smo.DatabaseCollection = ClientManager.EnumerateLocalDatabases(instanceName)

            If databases Is Nothing OrElse databases.Count <= 0 Then
                My.Application.Log.WriteEntry("Databases not found.", TraceEventType.Critical)
                Return False
            End If

            Dim affirmative As Boolean
            affirmative = databases.Contains(catalogName)

            'For Each database As Microsoft.SqlServer.Management.Smo.Database In databases
            ' If String.Compare(database.Name, catalogName, True, System.Globalization.CultureInfo.CurrentCulture) = 0 Then
            'Return True
            'End If
            'Next

            If Not affirmative Then
                ' if not located, report all the server that were found
                My.Application.Log.WriteEntry("Failed locating database.  System reports the following databases:", TraceEventType.Critical)
                For Each database As Microsoft.SqlServer.Management.Smo.Database In databases
                    My.Application.Log.WriteEntry(database.Name, TraceEventType.Critical)
                Next
            End If
            Return affirmative

        End Function

        Private Shared _localInstanceName As String
        ''' <summary>
        ''' Returns the local instance name.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared ReadOnly Property LocalInstanceName() As String
            Get
                If String.IsNullOrEmpty(_localInstanceName) Then
                    Dim server As New Microsoft.SqlServer.Management.Smo.Server(System.Environment.MachineName)
                    If server IsNot Nothing Then
                        _localInstanceName = server.Name
                    Else
                        _localInstanceName = String.Empty
                    End If
                End If
                Return _localInstanceName
            End Get
        End Property

        Private _verificationMessage As String = ""
        Public ReadOnly Property VerificationMessage() As String
            Get
                Return _verificationMessage
            End Get
        End Property

#End Region

#Region " RESTORE "

        ''' <summary>
        ''' Restore the specified data base from the database backup file.
        ''' </summary>
        ''' <param name="instanceName">Specifies the local server instance name, such as 'SQLEXPRESS'</param>
        ''' <param name="databaseName">Specifies the database name.</param>
        ''' <param name="restoreFilePathName">Specifies the file name path of the database backup to restore.</param>
        ''' <remarks></remarks>
        Public Shared Function RestoreLocalDatabase(ByVal instanceName As String, _
            ByVal databaseName As String, _
            ByVal restoreFilePathName As String) As Integer

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            If String.IsNullOrEmpty(databaseName) Then
                Throw New ArgumentNullException("databaseName")
            End If

            If String.IsNullOrEmpty(restoreFilePathName) Then
                Throw New ArgumentNullException("restoreFilePathName")
            End If

            If Not System.IO.File.Exists(restoreFilePathName) Then
                Throw New System.IO.FileNotFoundException("Failed locating restore file.", restoreFilePathName)
            End If

            If isr.Data.Sql.ClientManager.IsLocalCatalogExists(instanceName, databaseName) Then
                Throw New isr.Data.Sql.DatabaseAlreadyExistsException("Cannot restore over an existing database. Database must be dropped first.", instanceName, databaseName)
            End If

            Dim cmd As System.Data.SqlClient.SqlCommand = Nothing
            Dim conn As System.Data.SqlClient.SqlConnection = Nothing

            Try

                ' connect to the master database.
                conn = ClientManager.ConnectLocalDatabase(instanceName, "master")

                My.Application.Log.WriteEntry("Restoring database: " & databaseName, TraceEventType.Verbose)
                My.Application.Log.WriteEntry("From: " & restoreFilePathName, TraceEventType.Verbose)

                Dim queryFormat As New System.Text.StringBuilder

                ' Use WITH NORECOVERY if you plan to also restore a differential database backup
                queryFormat.Append("restore database [{0}] from disk='{1}' with recovery, replace")

                Dim query As String = String.Format(Globalization.CultureInfo.CurrentCulture, _
                    queryFormat.ToString, databaseName, restoreFilePathName)

                My.Application.Log.WriteEntry("Created query on the local SQL server:", TraceEventType.Verbose)
                My.Application.Log.WriteEntry(query, TraceEventType.Verbose)

                ' Create SQL query 
                cmd = New System.Data.SqlClient.SqlCommand(query, conn)
                If cmd.Connection.State <> ConnectionState.Open Then
                    cmd.Connection.Open()
                End If

                My.Application.Log.WriteEntry("Created command to execute on the local SQL server:", TraceEventType.Verbose)
                My.Application.Log.WriteEntry(cmd.CommandText, TraceEventType.Verbose)
                cmd.CommandType = System.Data.CommandType.Text

                My.Application.Log.WriteEntry("Executing command on the local SQL server:", TraceEventType.Verbose)
                RestoreLocalDatabase = cmd.ExecuteNonQuery()

                My.Application.Log.WriteEntry("Done creating database schema.", TraceEventType.Verbose)

            Finally

                If Not cmd Is Nothing Then
                    cmd.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If

            End Try

        End Function

        ''' <summary>
        ''' Restore the specified database on the local server instance.
        ''' </summary>
        ''' <param name="instanceName">Specifies the local server instance name, such as 'SQLEXPRESS'</param>
        ''' <param name="databaseName">Specifies the database name.</param>
        ''' <param name="restoreFolderPathName">Specifies the folder path of the database backup to restore.</param>
        ''' <param name="dataFolderPathName">Specifies the folder into which to restore the database.</param>
        ''' <remarks></remarks>
        Public Shared Sub RestoreLocalDatabase(ByVal instanceName As String, _
            ByVal databaseName As String, _
            ByVal restoreFolderPathName As String, _
            ByVal dataFolderPathName As String)

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            If String.IsNullOrEmpty(databaseName) Then
                Throw New ArgumentNullException("databaseName")
            End If

            If String.IsNullOrEmpty(restoreFolderPathName) Then
                Throw New ArgumentNullException("restoreFolderPathName")
            End If

            If String.IsNullOrEmpty(dataFolderPathName) Then
                Throw New ArgumentNullException("dataFolderPathName")
            End If

            Dim cmd As System.Data.SqlClient.SqlCommand = Nothing
            Dim conn As System.Data.SqlClient.SqlConnection = Nothing

            Try

                ' connect to the master database.
                conn = ClientManager.ConnectLocalDatabase(instanceName, "master")

                My.Application.Log.WriteEntry("Restoring database: " & databaseName, TraceEventType.Verbose)
                My.Application.Log.WriteEntry("From: " & restoreFolderPathName, TraceEventType.Verbose)
                My.Application.Log.WriteEntry("To: " & dataFolderPathName, TraceEventType.Verbose)

                Dim queryFormat As New System.Text.StringBuilder
                queryFormat.Append("restore database [{1}] from disk='{0}{1}.bak' ")

                ' Use WITH REPLACE in case the database already exists
                queryFormat.Append("with replace, ")

                ' Use WITH NORECOVERY if you plan to also restore a differential database backup
                queryFormat.Append("recovery, ")
                queryFormat.Append("MOVE '{1}_Data' TO '{2}\{1}_Data.MDF', ")
                queryFormat.Append("MOVE '{1}_Log' TO '{2}\{1}_Log.LDF'")

                Dim query As String = String.Format(Globalization.CultureInfo.CurrentCulture, _
                    queryFormat.ToString, restoreFolderPathName, databaseName, dataFolderPathName)

                My.Application.Log.WriteEntry("Created query on the local SQL server:", TraceEventType.Verbose)
                My.Application.Log.WriteEntry(query, TraceEventType.Verbose)

                ' Create SQL query 
                cmd = New System.Data.SqlClient.SqlCommand(query, conn)
                If cmd.Connection.State <> ConnectionState.Open Then
                    cmd.Connection.Open()
                End If

                My.Application.Log.WriteEntry("Created command to execute on the local SQL server:", TraceEventType.Verbose)
                My.Application.Log.WriteEntry(cmd.CommandText, TraceEventType.Verbose)
                cmd.CommandType = System.Data.CommandType.Text

                My.Application.Log.WriteEntry("Executing command on the local SQL server:", TraceEventType.Verbose)
                cmd.ExecuteNonQuery()

                My.Application.Log.WriteEntry("Done creating database schema.", TraceEventType.Verbose)

            Finally

                If Not cmd Is Nothing Then
                    cmd.Dispose()
                End If
                If Not conn Is Nothing Then
                    conn.Dispose()
                End If

            End Try

        End Sub

#End Region

#Region " COLUMNS "

        ''' <summary>Returns true if the specified column exists in the specified table.
        ''' </summary>
        Public Function ColumnExists(ByVal tableName As String, ByVal columnName As String) As Boolean
            If String.IsNullOrEmpty(tableName) Then
                Throw New ArgumentNullException("tableName")
            End If
            If String.IsNullOrEmpty(columnName) Then
                Throw New ArgumentNullException("columnName")
            End If
            Dim columnNames As String = Me.FetchColumnNames(tableName)
            Return columnNames.IndexOf(columnName, 0, StringComparison.OrdinalIgnoreCase) > 0
        End Function

        ''' <summary>Returns true if the specified column exists in the specified table.
        ''' </summary>
        Public Shared Function ColumnExists(ByVal connectionString As String, ByVal tableName As String, ByVal columnName As String) As Boolean
            If String.IsNullOrEmpty(connectionString) Then
                Throw New ArgumentNullException("connectionString")
            End If
            If String.IsNullOrEmpty(tableName) Then
                Throw New ArgumentNullException("tableName")
            End If
            If String.IsNullOrEmpty(columnName) Then
                Throw New ArgumentNullException("columnName")
            End If
            Dim columnNames As String = ClientManager.FetchColumnNames(connectionString, tableName)
            Return columnNames.IndexOf(columnName, 0, StringComparison.OrdinalIgnoreCase) > 0
        End Function

        ''' <summary>Gets the column names of the specified table.
        ''' </summary>
        Public Function FetchColumnNames(ByVal tableName As String) As String

            If String.IsNullOrEmpty(tableName) Then
                Throw New ArgumentNullException("tableName")
            End If

            Dim query As String
            'query = "select column_name,* from information_schema.columns where table_schema = '{0}' AND table_name = '{1}' order by ordinal_position"
            'query = String.Format(Globalization.CultureInfo.CurrentCulture, query, _databaseName, tableName)
            query = "select column_name from information_schema.columns where table_name = '{0}' order by ordinal_position"
            query = String.Format(Globalization.CultureInfo.CurrentCulture, query, tableName)

            Return Me.FetchColumnValues(query, 0)

        End Function

        ''' <summary>Gets the column names of the specified table.
        ''' </summary>
        Public Shared Function FetchColumnNames(ByVal connectionString As String, ByVal tableName As String) As String

            If String.IsNullOrEmpty(connectionString) Then
                Throw New ArgumentNullException("connectionString")
            End If

            If String.IsNullOrEmpty(tableName) Then
                Throw New ArgumentNullException("tableName")
            End If

            Dim query As String
            'query = "select column_name,* from information_schema.columns where table_schema = '{0}' AND table_name = '{1}' order by ordinal_position"
            'query = String.Format(Globalization.CultureInfo.CurrentCulture, query, _databaseName, tableName)
            query = "select column_name from information_schema.columns where table_name = '{0}' order by ordinal_position"
            query = String.Format(Globalization.CultureInfo.CurrentCulture, query, tableName)

            Return ClientManager.FetchColumnValues(connectionString, query, 0)

        End Function

        ''' <summary>Gets column values from the specified query.
        ''' </summary>
        ''' <param name="query"></param>
        ''' <param name="columnNumber">Zero-based column number</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function FetchColumnValues(ByVal query As String, ByVal columnNumber As Integer) As String

            If String.IsNullOrEmpty(query) Then
                Throw New ArgumentNullException("query")
            End If

            ' connect to the local database.
            Using conn As System.Data.SqlClient.SqlConnection = ClientManager.ConnectLocalDatabase(Me.InstanceName, Me._databaseName)
                Return ClientManager.FetchColumnValues(conn, query, columnNumber)
            End Using

        End Function

        ''' <summary>Gets column values from the specified query.
        ''' </summary>
        ''' <param name="connection"></param>
        ''' <param name="query"></param>
        ''' <param name="columnNumber">Zero-based column number</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function FetchColumnValues(ByVal connection As System.Data.SqlClient.SqlConnection, _
                                          ByVal query As String, ByVal columnNumber As Integer) As String

            If String.IsNullOrEmpty(query) Then
                Throw New ArgumentNullException("query")
            End If

            Dim lastAction As String

            ' Create SQL query 
            Using cmd As New System.Data.SqlClient.SqlCommand(query)

                cmd.Connection = connection
                cmd.CommandType = System.Data.CommandType.Text
                If connection.State = ConnectionState.Closed Then
                    connection.Open()
                End If

                lastAction = "Created command to execute on the local SQL server:"
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
                My.Application.Log.WriteEntry(cmd.CommandText, TraceEventType.Verbose)

                lastAction = "Executing command on the local SQL server..."
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
                Dim reader As SqlClient.SqlDataReader = cmd.ExecuteReader()

                Dim values As New System.Text.StringBuilder
                If reader.HasRows Then
                    Do While reader.Read()
                        If values.Length > 0 Then
                            values.Append(",")
                        End If
                        values.Append(reader.GetString(columnNumber))
                    Loop
                End If
                reader.Close()
                Return values.ToString
            End Using

        End Function

        ''' <summary>Gets column values from the specified query.
        ''' </summary>
        ''' <param name="connectionString">Specifies the connection string to connect to.</param>
        ''' <param name="query"></param>
        ''' <param name="columnNumber">Zero-based column number</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function FetchColumnValues(ByVal connectionString As String, ByVal query As String, ByVal columnNumber As Integer) As String

            If String.IsNullOrEmpty(query) Then
                Throw New ArgumentNullException("query")
            End If

            If String.IsNullOrEmpty(connectionString) Then
                Throw New ArgumentNullException("connectionString")
            End If

            ' connect to the local database.
            Using conn As New System.Data.SqlClient.SqlConnection(connectionString)
                conn.Open()
                Return ClientManager.FetchColumnValues(conn, query, columnNumber)
            End Using

        End Function

        ''' <summary>Gets all tables names.  
        ''' </summary>
        Public Function FetchTableNames() As String

            Dim query As String
            query = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES"
            My.Application.Log.WriteEntry("Created query on the local SQL server:", TraceEventType.Verbose)
            My.Application.Log.WriteEntry(query, TraceEventType.Verbose)

            Return Me.FetchColumnValues(query, 0)

        End Function

        ''' <summary>Gets all tables names.  
        ''' </summary>
        Public Shared Function FetchTableNames(ByVal connectionString As String) As String

            If String.IsNullOrEmpty(connectionString) Then
                Throw New ArgumentNullException("connectionString")
            End If

            Dim query As String
            query = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES"
            My.Application.Log.WriteEntry("Created query on the local SQL server:", TraceEventType.Verbose)
            My.Application.Log.WriteEntry(query, TraceEventType.Verbose)

            Return ClientManager.FetchColumnValues(connectionString, query, 0)

        End Function

        ''' <summary>
        ''' Renames a table column.
        ''' </summary>
        ''' <param name="tableName"></param>
        ''' <param name="currentName"></param>
        ''' <param name="newName"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="recordsAffected")> _
        Public Function RenameColumn(ByVal tableName As String, ByVal currentName As String, ByVal newName As String) As Boolean

            Dim query As String
            Dim recordsAffected As Long

            ' check if old column exists
            If Me.ColumnExists(tableName, currentName) Then

                ' drop the old column: We can do this because this table is still empty.
                ' query = "sp_rename '" & tableName & "." & currentName & "', '" & newName & "', 'COLUMN'"
                query = String.Format(Globalization.CultureInfo.CurrentCulture, _
                                      "sp_rename '{0}.{1}', '{2}', 'COLUMN'", tableName, currentName, newName)

                recordsAffected = ClientManager.UpdateTableQuery(Me.InstanceName, Me._databaseName, query)

            End If

            ' check if old column still exists
            If Me.ColumnExists(tableName, currentName) Then

                _verificationMessage = "Failed renaming '" & currentName & "' column."
                Return False

            ElseIf Not Me.ColumnExists(tableName, newName) Then

                _verificationMessage = "Failed adding '" & newName & "' column."
                Return False

            Else

                Return True

            End If

        End Function

        ''' <summary>Returns true if the specified Table exists in the current database.
        ''' </summary>
        Public Function TableExists(ByVal tableName As String) As Boolean
            If String.IsNullOrEmpty(tableName) Then
                Throw New ArgumentNullException("tableName")
            End If
            Dim TableNames As String = Me.FetchTableNames()
            Return TableNames.IndexOf(tableName, 0, StringComparison.OrdinalIgnoreCase) > 0
        End Function

        ''' <summary>Returns true if the specified Table exists in the specified database.
        ''' </summary>
        Public Shared Function TableExists(ByVal connectionString As String, ByVal tableName As String) As Boolean
            If String.IsNullOrEmpty(connectionString) Then
                Throw New ArgumentNullException("connectionString")
            End If
            If String.IsNullOrEmpty(tableName) Then
                Throw New ArgumentNullException("tableName")
            End If
            Dim TableNames As String = ClientManager.FetchTableNames(connectionString)
            Return TableNames.IndexOf(tableName, 0, StringComparison.OrdinalIgnoreCase) > 0
        End Function

#End Region

#Region " TABLES "

        ''' <summary>
        ''' Executes an update query on a table.
        ''' </summary>
        ''' <param name="query"></param>
        ''' <returns>The number of affected records.</returns>
        ''' <remarks></remarks>
        Public Shared Function UpdateTableQuery(ByVal connectionString As String, ByVal query As String) As System.Int32

            If String.IsNullOrEmpty(connectionString) Then
                Throw New ArgumentNullException("connectionString")
            End If

            If String.IsNullOrEmpty(query) Then
                Throw New ArgumentNullException("query")
            End If

            Dim lastAction As String
            Dim cmd As System.Data.SqlClient.SqlCommand = Nothing

            Try

                lastAction = "Creating command on the local SQL server..."
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)

                ' Create SQL query 
                cmd = New System.Data.SqlClient.SqlCommand(query)
                cmd.CommandType = System.Data.CommandType.Text

                My.Application.Log.WriteEntry("Creating SQL Connection", TraceEventType.Verbose)
                cmd.Connection = New System.Data.SqlClient.SqlConnection(connectionString)
                If cmd.Connection.State <> ConnectionState.Open Then
                    cmd.Connection.Open()
                End If

                lastAction = "Created command to execute on the local SQL server:"
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
                My.Application.Log.WriteEntry(cmd.CommandText, TraceEventType.Verbose)

                lastAction = "Executing command on the local SQL server..."
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
                Return cmd.ExecuteNonQuery()

            Finally

                If Not cmd Is Nothing Then
                    cmd.Dispose()
                    cmd = Nothing
                End If

            End Try

        End Function

        ''' <summary>
        ''' Executes an update query on a table.
        ''' </summary>
        ''' <param name="instanceName"></param>
        ''' <param name="databaseName"></param>
        ''' <param name="query"></param>
        ''' <returns>The number of affected records.</returns>
        ''' <remarks></remarks>
        Public Shared Function UpdateTableQuery(ByVal instanceName As String, _
            ByVal databaseName As String, ByVal query As String) As System.Int32

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            If String.IsNullOrEmpty(databaseName) Then
                Throw New ArgumentNullException("databaseName")
            End If

            If String.IsNullOrEmpty(query) Then
                Throw New ArgumentNullException("query")
            End If

            Dim lastAction As String
            Dim cmd As System.Data.SqlClient.SqlCommand = Nothing

            Try

                lastAction = "Creating command on the local SQL server..."
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)

                ' Create SQL query 
                cmd = New System.Data.SqlClient.SqlCommand(query)
                cmd.CommandType = System.Data.CommandType.Text

                lastAction = "Opening connection"
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
                cmd.Connection = ClientManager.ConnectLocalDatabase(instanceName, databaseName)
                If cmd.Connection.State <> ConnectionState.Open Then
                    cmd.Connection.Open()
                End If

                lastAction = "Created command to execute on the local SQL server:"
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
                My.Application.Log.WriteEntry(cmd.CommandText, TraceEventType.Verbose)

                lastAction = "Executing command on the local SQL server..."
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
                Return cmd.ExecuteNonQuery()

            Finally

                If Not cmd Is Nothing Then
                    cmd.Dispose()
                    cmd = Nothing
                End If

            End Try

        End Function

        ''' <summary>
        ''' Fetches a record from the database using the specified query.
        ''' </summary>
        ''' <param name="query"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function FetchRecord(ByVal query As String) As System.Data.SqlClient.SqlDataReader

            Dim lastAction As String

            lastAction = "fetching record from '" & _databaseName & "' on '" & Me.InstanceName & "'."
            OnMessageAvailable(lastAction, False, lastAction)

            ' backup and get the backup file name.
            Return isr.Data.Sql.ClientManager.FetchRecord(Me.InstanceName, _databaseName, query)

        End Function

#End Region





#Region " DATABASE "


        ''' <summary>
        ''' Detaches the current database.
        ''' </summary>
        ''' <remarks></remarks>
        Public Function DetachDatabase() As Boolean

            'Me.LocateServerAndDatabase()

            Dim lastAction As String
            lastAction = "Detaching '" & _databaseName & "' on '" & Me.InstanceName & "'."
            OnMessageAvailable(lastAction, False, lastAction)
            My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)

            ' detaches the database
            isr.Data.Sql.ClientManager.DetachLocalDatabase(Me.InstanceName, _databaseName)

            Return True

        End Function

        ''' <summary>
        ''' Drops the current database.
        ''' </summary>
        ''' <remarks></remarks>
        Public Function DropDatabase() As Boolean

            'Me.LocateServerAndDatabase()

            Dim lastAction As String

            lastAction = "Dropping '" & _databaseName & "' on '" & Me.InstanceName & "'."
            OnMessageAvailable(lastAction, False, lastAction)

            ' backup and get the backup file name.
            lastAction = isr.Data.Sql.ClientManager.DropLocalDatabase(Me.InstanceName, _databaseName)
            lastAction = "Dropped " & lastAction
            OnMessageAvailable(lastAction, False, lastAction)
            My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)

            _isDatabaseExists = False

            Return True

        End Function

        Private _isDatabaseExists As Boolean
        ''' <summary>
        ''' Gets true if the database exists.  The database must be located first
        ''' using <see cref="LocateDatabase"/>.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property IsDatabaseExists() As Boolean
            Get
                Return _isDatabaseExists
            End Get
        End Property

        ''' <summary>
        ''' Throws exceptions if database or server do not exist.
        ''' </summary>
        ''' <remarks></remarks>
        Public Function LocateServerAndDatabase() As Boolean

            If Not Me.LocateDatabase Then
                If Me.LocateLocalServer Then
                    Throw New isr.Data.Sql.LocalServerNotFoundException()
                Else
                    Throw New isr.Data.Sql.DatabaseNotFoundException(Me.InstanceName, _databaseName)
                End If
            End If

            Return True

        End Function

        ''' <summary>
        ''' Looks for the the database and returns true if exists.  Sets the local cache 
        ''' <see cref="IsDatabaseExists"/> for the condition.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function LocateDatabase(ByVal connectionString As String) As Boolean
            Dim lastAction As String
            lastAction = "Locating database."
            OnMessageAvailable(lastAction, False, lastAction)
            My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
            _isDatabaseExists = isr.Data.Sql.ClientManager.IsCatalogExists(connectionString)
            Return _isDatabaseExists
        End Function

        ''' <summary>
        ''' Looks for the the database and returns true if exists.  Sets the local cache 
        ''' <see cref="IsDatabaseExists"/> for the condition.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function LocateDatabase() As Boolean
            Dim lastAction As String
            lastAction = "Locating database."
            OnMessageAvailable(lastAction, False, lastAction)
            My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
            _isDatabaseExists = isr.Data.Sql.ClientManager.IsLocalCatalogExists(Me.InstanceName, _databaseName)
            Return _isDatabaseExists
        End Function

        ''' <summary>Returns true if the database exists.
        ''' </summary>
        Public Function LocateDatabaseVerbose() As Boolean

            Dim lastAction As String

            lastAction = "Locating '" & Me.DatabaseName & "' database..."
            OnMessageAvailable(lastAction, False, lastAction)
            My.Application.Log.WriteEntry(lastAction, TraceEventType.Information)
            If Me.IsDatabaseExists Then

                lastAction = "Located '" & Me.DatabaseName & "' database."
                OnMessageAvailable(lastAction, False, lastAction)
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Information)

            Else

                ' if database not found, see if we have server.
                Me.LocateLocalServer()

                If Me.IsLocalServerExists Then

                    lastAction = "Database '" & Me.DatabaseName & "' not found on local server '" & isr.Data.Sql.ClientManager.LocatedServerName
                    OnMessageAvailable(lastAction, False, lastAction)
                    My.Application.Log.WriteEntry(lastAction, TraceEventType.Information)

                Else

                    lastAction = "Local server not found on '" & Environment.MachineName & "'."
                    OnMessageAvailable(lastAction, False, lastAction)
                    My.Application.Log.WriteEntry(lastAction, TraceEventType.Information)

                End If

            End If

        End Function

        ''' <summary>
        ''' Restores the database.
        ''' </summary>
        ''' <remarks></remarks>
        Public Function RestoreDatabase() As Boolean

            Return Me.RestoreDatabase(Me.RestoreFilePathName)

        End Function

        ''' <summary>
        ''' Restores the database.
        ''' </summary>
        ''' <remarks></remarks>
        Public Function RestoreDatabase(ByVal filePathName As String) As Boolean

            ' make sure database not located.
            If Me.LocateDatabase Then
                Throw New isr.Data.Sql.DatabaseAlreadyExistsException(Me.InstanceName, _databaseName)
                ' make sure server exists.
            ElseIf Not Me.LocateLocalServer() Then
                Throw New isr.Data.Sql.LocalServerNotFoundException()
            End If

            Dim lastAction As String

            lastAction = "Restoring database on the local SQL server."
            OnMessageAvailable(lastAction, False, lastAction)
            My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
            isr.Data.Sql.ClientManager.RestoreLocalDatabase(Me.InstanceName, _databaseName, filePathName)

            Return True

        End Function

        ''' <summary>
        ''' Updates the database.
        ''' </summary>
        ''' <remarks></remarks>
        Public Function UpdateDatabase() As Boolean

            'Me.LocateServerAndDatabase()

            Dim lastAction As String

            lastAction = "Doing a final backup..."
            OnMessageAvailable(lastAction, False, lastAction)
            My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
            Me.BackupDatabase()

            lastAction = "Archiving final backup..."
            OnMessageAvailable(lastAction, False, lastAction)
            My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
            Me.ArchiveBackup()

            lastAction = "Dropping database..."
            OnMessageAvailable(lastAction, False, lastAction)
            My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
            Me.DropDatabase()

            lastAction = "Restoring database..."
            OnMessageAvailable(lastAction, False, lastAction)
            My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
            Me.RestoreDatabase()

            lastAction = "Doing first backup..."
            OnMessageAvailable(lastAction, False, lastAction)
            My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
            Return Me.BackupDatabase()

        End Function

#End Region

#Region " DATA MEMBERS "

        Private _backupFileName As String
        ''' <summary>
        ''' Gets the backup file name for full and differential backups.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property BackupFileName() As String
            Get
                If String.IsNullOrEmpty(_backupFileName) Then
                    _backupFileName = _databaseName & _backupFileSuffix & _defaultBackupFileExtension
                End If
                Return _backupFileName
            End Get
            Set(ByVal value As String)
                _backupFileName = value
            End Set
        End Property

        Private _backupFileSuffix As String = "-Diff"
        ''' <summary>
        ''' Gets or sets the suffix to add to the backup file name.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property BackupFileSuffix() As String
            Get
                Return _backupFileSuffix
            End Get
            Set(ByVal value As String)
                _backupFileSuffix = value
            End Set
        End Property

        ''' <summary>
        ''' Gets the full and differential backup file path name from which to restore the database.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property BackupFilePathName() As String
            Get
                Return System.IO.Path.Combine(_backupFolderPathName, Me.BackupFileName)
            End Get
        End Property

        Private _backupFolderPathName As String ' make sure we get it from the server = "C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Backup"
        ''' <summary>
        ''' Gets or sets the folder path name where the database backup is located.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property BackupFolderPathName() As String
            Get
                Return _backupFolderPathName
            End Get
            Set(ByVal value As String)
                _backupFolderPathName = value
            End Set
        End Property

        Private _backupName As String
        ''' <summary>
        ''' Gets or sets the backup name.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property BackupName() As String
            Get
                Return _backupName
            End Get
            Set(ByVal value As String)
                _backupName = value
            End Set
        End Property

        Private _dataParentPathName As String ' make sure we get it from the server = "C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL"
        ''' <summary>
        ''' Gets or sets the top level path of the database.  The data base is located in the
        ''' data folder of the path and the database backup is in the backup folder of this path.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property DataParentPathName() As String
            Get
                If String.IsNullOrEmpty(_dataParentPathName) Then
                    Me._dataParentPathName = ClientManager.FetchDataParentPathName(Me.InstanceName)
                End If
                Return _dataParentPathName
            End Get
        End Property

        Private _databaseName As String
        ''' <summary>
        ''' Gets or sets the database name.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property DatabaseName() As String
            Get
                Return _databaseName
            End Get
            Set(ByVal value As String)
                _databaseName = value
            End Set
        End Property

        ''' <summary>
        ''' Gets the default connection string based.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property DefaultConnectionString() As String
            Get
                Return isr.Data.Sql.ClientManager.BuildLocalConnectionStringLlblgen(Me.InstanceName, _databaseName)
            End Get
        End Property

        ''' <summary>
        ''' Gets the folder where the backup database file resides.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property DefaultBackupFolderPathName() As String
            Get
                Return System.IO.Path.Combine(Me.DataParentPathName, "Backup")
            End Get
        End Property

        ''' <summary>
        ''' Gets the folder where the database file resides.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property DefaultDataFolderPathName() As String
            Get
                Return System.IO.Path.Combine(Me.DataParentPathName, "Data")
            End Get
        End Property

        Private _instanceName As String = "SQLEXPRESS"
        ''' <summary>
        ''' Gets or sets the instance name, e.g., 'SQLEXPRESS'.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property InstanceName() As String
            Get
                Return _instanceName
            End Get
            Set(ByVal value As String)
                Me._dataParentPathName = ClientManager.FetchDataParentPathName(Me._instanceName)
                Me.BackupFolderPathName = Me.DefaultBackupFolderPathName
                Me.RestoreFolderPathName = Me.DefaultBackupFolderPathName
                _instanceName = value
            End Set
        End Property

        Private _isLocalServerExists As Boolean
        ''' <summary>
        ''' Gets true if the local server exists.  The server must be located first
        ''' using <see cref="LocateLocalServer"/>.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property IsLocalServerExists() As Boolean
            Get
                Return _isLocalServerExists
            End Get
        End Property

        ''' <summary>
        ''' Returns true if a sql server exists on the local machine.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function LocateLocalServer() As Boolean

            Dim server As New Microsoft.SqlServer.Management.Smo.Server(System.Environment.MachineName)
            _isLocalServerExists = server IsNot Nothing
            ' reset the database located status if server not found.
            _isDatabaseExists = _isDatabaseExists And _isLocalServerExists
            Return _isLocalServerExists

        End Function

        Private _lastTimeStampedBackupFilePathName As String
        ''' <summary>
        ''' Gets the last backup archive file name.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property LastTimeStampedBackupFilePathName() As String
            Get
                Return _lastTimeStampedBackupFilePathName
            End Get
        End Property

        Private _restoreFileSuffix As String = "-Master"
        ''' <summary>
        ''' Gets or sets the suffix for the master database backup name.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property RestoreFileSuffix() As String
            Get
                Return _restoreFileSuffix
            End Get
            Set(ByVal value As String)
                _restoreFileSuffix = value
            End Set
        End Property

        Private _restoreFileName As String
        ''' <summary>
        ''' Gets the restore file name.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property RestoreFileName() As String
            Get
                If String.IsNullOrEmpty(_restoreFileName) Then
                    Return _databaseName & _restoreFileSuffix & _defaultBackupFileExtension
                Else
                    Return _restoreFileName
                End If
            End Get
            Set(ByVal value As String)
                _restoreFileName = value
            End Set
        End Property

        ''' <summary>
        ''' Gets the restore file path name.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property RestoreFilePathName() As String
            Get
                Return System.IO.Path.Combine(Me.RestoreFolderPathName, Me.RestoreFileName)
            End Get
        End Property

        Private _restoreFolderPathName As String
        ''' <summary>
        ''' Gets or sets the folder path name where the database backup for restore is located.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property RestoreFolderPathName() As String
            Get
                Return _restoreFolderPathName
            End Get
            Set(ByVal value As String)
                _restoreFolderPathName = value
            End Set
        End Property

        ''' <summary>
        ''' Gets the local server version.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property ServerVersion() As System.Version
            Get
                Return isr.Data.Sql.ClientManager.FetchLocalServerVersion(Me.InstanceName)
            End Get
        End Property

        Private _timestampFormat As String = "-{0:yyyyMMdd}{0:HHmmss}"
        ''' <summary>
        ''' Gets or sets the time stamp format string.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property TimestampFormat() As String
            Get
                Return _timestampFormat
            End Get
            Set(ByVal value As String)
                _timestampFormat = value
            End Set
        End Property

        ''' <summary>
        ''' Gets the time stamped file path name from which to restore the database.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property TimestampBackupFilePathName() As String
            Get
                Return System.IO.Path.Combine(_backupFolderPathName, Me.BuildTimeStampedBackupFileName)
            End Get
        End Property

#End Region

#Region " EVENTS "

        ''' <summary>The event is raised when the bank as a message to log or display.</summary>
        ''' <param name="e">A reference to the <see cref="T:System.EventArgs"/> event arguments</param>
        ''' <remarks>Must be declared the old way to work with COM.</remarks>
        Public Event MessageAvailable As EventHandler(Of Sql.EventArgs)

        ''' <summary>The event is raised when an error occurred.</summary>
        ''' <param name="e">A reference to the <see cref="T:System.EventArgs"/> event arguments</param>
        ''' <remarks>Must be declared the old way to work with COM.</remarks>
        Public Event ErrorAvailable As EventHandler(Of Sql.EventArgs)

#End Region

#Region " ON EVENT HANDLERS "

        ''' <summary>Logs a message using the COM calling object.  
        ''' Under COM the library cannot log message.</summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="dummy")> _
        Friend Sub OnMessageAvailable(ByVal synopsis As String, ByVal dummy As Boolean, ByVal message As String)
            If message Is Nothing Then
                Throw New ArgumentNullException("message")
            End If
            If synopsis Is Nothing Then
                Throw New ArgumentNullException("synopsis")
            End If
            Dim e As New Sql.EventArgs(False, synopsis, message)
            If MessageAvailableEvent IsNot Nothing Then MessageAvailableEvent(Me, e)
        End Sub

#End Region

#Region " NOT USED "
#If False Then

        ''' <summary>Aborts all proofs and raises the bank failed event.</summary>
        ''' <param name="ex">A reference to the <see cref="system.Exception"/>exception</param>
        Friend Sub OnErrorAvailable(ByVal ex As Exception, ByVal synopsis As String)
            If synopsis Is Nothing Then
                Throw New ArgumentNullException("synopsis")
            End If
            If ex Is Nothing Then
                Throw New ArgumentNullException("ex")
            End If
            Dim e As New Sql.EventArgs(ex, synopsis)
            If ErrorAvailableEvent IsNot Nothing Then ErrorAvailableEvent(Me, e)
        End Sub

        ''' <summary>
        ''' Drops the database and move the database files to the archive folder under
        ''' the main SQL parent folder.  
        ''' </summary>
        ''' <param name="pathTitle">Specifies the backup folder path title, e.g., 'Backup'</param>
        ''' <remarks>Prior to calling this method, make sure to upload existing proofs to the remote server.</remarks>
        Public Function ArchiveDatabase(ByVal pathTitle As String) As Boolean

            Dim lastAction As String

            If String.IsNullOrEmpty(pathTitle) Then
                Throw New ArgumentNullException("pathTitle")
            End If

            Try

                lastAction = "Archiving local database: ' " & _databaseName & "' on '" & Me.InstanceName & "'."
                Me.OnMessageAvailable(lastAction)
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)

                lastAction = isr.Data.Sql.ClientManager.ArchiveLocalDatabase(Me.InstanceName, _databaseName, pathTitle)

                If lastAction.Contains("\") Then
                    ArchiveDatabase = True
                    lastAction = "Database file archived to '" & lastAction & "'"
                    Me.OnMessageAvailable(lastAction)
                    My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
                Else
                    ArchiveDatabase = False
                    lastAction = "Failed archiving database. " & lastAction
                    Me.OnMessageAvailable(lastAction)
                    My.Application.Log.WriteEntry(lastAction, TraceEventType.Warning)
                End If

            Catch ex As System.Data.SqlClient.SqlException

                lastAction = "Failed archiving database."
                Me.OnErrorAvailable(ex)

                ' log the error message.
                My.MyApplication.ProcessException(ex, lastAction, WindowsForms.ExceptionDisplayButtons.Continue)

                Throw

            End Try

        End Function

        ''' <summary>
        ''' Removes the database from the SQL server and moves its file to the 
        ''' specified archive folder.
        ''' </summary>
        ''' <param name="instanceName">Specifies the local server instance name, such as 'SQLEXPRESS'</param>
        ''' <param name="catalogName">Specifies the database name.</param>
        ''' <param name="archivePathTitle">Specifies the archive folder path title, e.g., 'Backup'</param>
        ''' <returns>The destination file path name for the database file.</returns>
        ''' <remarks></remarks>
        Public Shared Function ArchiveLocalDatabase(ByVal instanceName As String, _
          ByVal catalogName As String, ByVal archivePathTitle As String) As String

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            If String.IsNullOrEmpty(catalogName) Then
                Throw New ArgumentNullException("catalogName")
            End If

            If String.IsNullOrEmpty(archivePathTitle) Then
                Throw New ArgumentNullException("archivePathTitle")
            End If

            Dim lastAction As String
            Dim databases As Microsoft.SqlServer.Management.Smo.DatabaseCollection = ClientManager.EnumerateLocalDatabases(instanceName)

            If databases Is Nothing OrElse databases.Count <= 0 Then
                My.Application.Log.WriteEntry("Databases not found.", TraceEventType.Critical)
            End If

            Dim fileList As New System.Collections.Generic.List(Of String)

            Dim database As Microsoft.SqlServer.Management.Smo.Database
            If databases.Contains(catalogName) Then

                database = databases.Item(catalogName)

                ' save the database files.
                For j As Integer = 0 To database.FileGroups.Count - 1
                    For i As Integer = 0 To database.FileGroups(j).Files.Count - 1
                        fileList.Add(database.FileGroups(j).Files(i).FileName)
                    Next
                Next
                For i As Integer = 0 To database.LogFiles.Count - 1
                    fileList.Add(database.LogFiles(i).FileName)
                Next

                ' backup the file to the \Archive forlder plus time stamp.
                Dim destination As String
                destination = ClientManager.BackupLocalDatabase(instanceName, catalogName, archivePathTitle, True)

                ' get the archive folder name
                Dim fi As New System.IO.FileInfo(destination)
                Dim archiveFolder As String = fi.Directory.ToString

                ' create destination file list.
                Dim destFileList As New System.Collections.Generic.List(Of String)
                For Each filename As String In fileList
                    destFileList.Add(System.IO.Path.Combine(archiveFolder, _
                        filename.Substring(database.PrimaryFilePath.Length + 1)))
                Next

                lastAction = "Detaching database  '{0}'."
                lastAction = String.Format(Globalization.CultureInfo.CurrentCulture, lastAction, database.Name)
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)

                ' detach the database
                ClientManager.DetachLocalDatabase(instanceName, catalogName)

                ' archive the database files.
                Dim sourceFilename As String
                Dim destFilename As String
                For i As Integer = 0 To destFileList.Count - 1
                    sourceFilename = fileList.Item(i)
                    destFilename = destFileList.Item(i)
                    lastAction = "Archiving file '{0}' to '{1}'."
                    lastAction = String.Format(Globalization.CultureInfo.CurrentCulture, lastAction, sourceFilename, destFilename)
                    My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
                    System.IO.File.Move(sourceFilename, destFilename)
                Next
                destFilename = destFileList.Item(0)

                ' lastAction = "Dropping database  '{0}'."
                ' lastAction = String.Format(Globalization.CultureInfo.CurrentCulture, lastAction, database.Name)
                ' My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)

                ' drop the database
                ' database.Drop()

                Return destFilename

            Else

                My.Application.Log.WriteEntry("Failed archiving database because the database does not exist.  System reports the following databases:", TraceEventType.Critical)
                For Each database In databases
                    My.Application.Log.WriteEntry(database.Name, TraceEventType.Critical)
                Next
                Return "Archive failed"

            End If

        End Function

        ''' <summary>
        ''' Backup the specified database on the local server instance with a timestamp.
        ''' </summary>
        ''' <param name="instanceName">Specifies the local server instance name, such as 'SQLEXPRESS'</param>
        ''' <param name="databaseName">Specifies the database name.</param>
        ''' <param name="pathTitle">Specifies the backup folder path title, e.g., 'Backup'</param>
        ''' <param name="useTimeTampedFolder">Specifies the condition for determining if a time stamped folder is to be
        ''' created for the backup file.</param>
        ''' <returns>The backup file path name.</returns>
        ''' <remarks></remarks>
        Public Shared Function BackupLocalDatabase(ByVal instanceName As String, _
            ByVal databaseName As String, ByVal pathTitle As String, ByVal useTimeTampedFolder As Boolean) As String

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            If String.IsNullOrEmpty(databaseName) Then
                Throw New ArgumentNullException("databaseName")
            End If

            If String.IsNullOrEmpty(pathTitle) Then
                Throw New ArgumentNullException("pathTitle")
            End If

            ' build the backup file name using the database name.
            Dim fileName As String
            fileName = "{0}.bak"
            fileName = String.Format(Globalization.CultureInfo.CurrentCulture, _
                fileName, databaseName)

            ' build the backup folder name.
            Dim backupFilePathName As String
            backupFilePathName = ClientManager.BuildBacupFolderPathName(instanceName, pathTitle, useTimeTampedFolder)
            backupFilePathName = System.IO.Path.Combine(backupFilePathName, fileName)

            ' call the general backup method. 
            Return ClientManager.BackupLocalDatabase(instanceName, databaseName, backupFilePathName)

        End Function

        ''' <summary>
        ''' Backs up the local database .
        ''' </summary>
        ''' <param name="instanceName"></param>
        ''' <param name="catalogName"></param>
        ''' <returns></returns>
        ''' <remarks>
        ''' backupSetDescription = "Full daily backup of AdventureWorks"
        ''' backupSetName = "AdventureWorks Backup"
        ''' backupName "Test_Full_Backup1"
        ''' </remarks>
        Public Shared Function BackupLocalDatabaseSmo(ByVal instanceName As String, _
          ByVal catalogName As String, ByVal backupName As String, _
          ByVal backupSetDescription As String, ByVal backupSetName As String) As Boolean

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            If String.IsNullOrEmpty(catalogName) Then
                Throw New ArgumentNullException("catalogName")
            End If

            Dim lastAction As String

            Dim conn As System.Data.SqlClient.SqlConnection = Nothing
            Try

                ' connect to the master database.
                conn = ClientManager.ConnectLocalDatabase(instanceName, "master")

                Dim svrConnection As New Microsoft.SqlServer.Management.Common.ServerConnection(conn)
                Dim svr As New Microsoft.SqlServer.Management.Smo.Server(svrConnection)

                If svr.Databases Is Nothing OrElse svr.Databases.Count <= 0 Then
                    My.Application.Log.WriteEntry("Databases not found.", TraceEventType.Critical)
                    Return False
                End If

                Dim database As Microsoft.SqlServer.Management.Smo.Database
                If svr.Databases.Contains(catalogName) Then

                    database = svr.Databases.Item(catalogName)

                    ' Store the current recovery model in a variable.
                    Dim recoverymod As Microsoft.SqlServer.Management.Smo.RecoveryModel
                    recoverymod = database.DatabaseOptions.RecoveryModel

                    ' Define a Backup object variable. 
                    Dim bk As New Microsoft.SqlServer.Management.Smo.Backup

                    ' Specify the type of backup, the description, the name, and the database to be backed up.
                    bk.Action = Microsoft.SqlServer.Management.Smo.BackupActionType.Database
                    bk.BackupSetDescription = backupSetDescription
                    bk.BackupSetName = backupSetName
                    bk.Database = catalogName

                    'Declare a BackupDeviceItem by supplying the backup device file name in the constructor, and the type of device is a file.
                    Dim bdi As Microsoft.SqlServer.Management.Smo.BackupDeviceItem
                    bdi = New Microsoft.SqlServer.Management.Smo.BackupDeviceItem(backupName, _
                        Microsoft.SqlServer.Management.Smo.DeviceType.File)

                    ' Add the device to the Backup object.
                    bk.Devices.Add(bdi)

                    ' Set the Incremental property to False to specify that this is a full database backup.
                    bk.Incremental = False

                    ' Set the expiration date.
                    bk.ExpirationDate = Now.AddYears(2)

                    ' Specify that the log must be truncated after the backup is complete.
                    bk.LogTruncation = Microsoft.SqlServer.Management.Smo.BackupTruncateLogType.Truncate

                    ' Run SqlBackup to perform the full database backup on the instance of SQL Server.
                    bk.SqlBackup(svr)

                    ' Inform the user that the backup has been completed.
                    lastAction = "Full Backup complete."
                    My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)

                    ' Remove the backup device from the Backup object.
                    bk.Devices.Remove(bdi)

#If False Then
          ' Create another file device for the differential backup and add the Backup object.
          Dim bdid As Microsoft.SqlServer.Management.Smo.BackupDeviceItem
          bdid = New Microsoft.SqlServer.Management.Smo.BackupDeviceItem("Test_Differential_Backup1", DeviceType.File)

          ' Add the device to the Backup object.
          bk.Devices.Add(bdid)

          ' Set the Incremental property to True for a differential backup.
          bk.Incremental = True

          ' Run SqlBackup to perform the incremental database backup on the instance of SQL Server.
          bk.SqlBackup(svr)

          'Inform the user that the differential backup is complete.
          Console.WriteLine("Differential Backup complete.")
          'Remove the device from the Backup object.
          bk.Devices.Remove(bdid)
          'Delete the AdventureWorks database before restoring it.
          srv.Databases("AdventureWorks").Drop()
#End If

                    ' Define a Restore object variable.
                    Dim rs As Microsoft.SqlServer.Management.Smo.Restore
                    rs = New Microsoft.SqlServer.Management.Smo.Restore

                    ' Set the NoRecovery property to true, so the transactions are not recovered.
                    rs.NoRecovery = True

                    ' Add the device that contains the full database backup to the Restore object.
                    rs.Devices.Add(bdi)

                    ' Specify the database name.
                    rs.Database = catalogName

                    ' Restore the full database backup with no recovery.
                    rs.SqlRestore(svr)

                    ' Inform the user that the Full Database Restore is complete.
                    lastAction = "Full Database Restore complete."
                    My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)

                    ' Remove the device from the Restore object.
                    rs.Devices.Remove(bdi)

                    ' Set te NoRecovery property to False.
                    rs.NoRecovery = False

#If False Then
          ' Create another file device for the differential backup and add the Backup object.
          Dim bdid As Microsoft.SqlServer.Management.Smo.BackupDeviceItem
          bdid = New Microsoft.SqlServer.Management.Smo.BackupDeviceItem("Test_Differential_Backup1", DeviceType.File)

          ' Add the device that contains the differential backup to the Restore object.
          rs.Devices.Add(bdid)

          ' Restore the differential database backup with recovery.
          rs.SqlRestore(svr)

          ' Inform the user that the differential database restore is complete.
          Console.WriteLine("Differential Database Restore complete.")

          ' Remove the device.
          rs.Devices.Remove(bdid)
#End If

                    ' Set the database recovery mode back to its original value.
                    svr.Databases("AdventureWorks").DatabaseOptions.RecoveryModel = recoverymod
#If False Then
          ' Remove the backup files from the hard disk.
          My.Computer.FileSystem.DeleteFile("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Backup\Test_Full_Backup1")
          My.Computer.FileSystem.DeleteFile("C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Backup\Test_Differential_Backup1")
#End If

                Else

                    My.Application.Log.WriteEntry("Failed backing up database because the database does not exist.  System reports the following databases:", TraceEventType.Critical)
                    For Each database In svr.Databases
                        My.Application.Log.WriteEntry(database.Name, TraceEventType.Critical)
                    Next
                    Return False

                End If

            Finally

                If Not conn Is Nothing Then
                    conn.Dispose()
                End If

            End Try

        End Function

        ''' <summary>
        ''' Gets the backup folder path name.
        ''' Gets a time stamped backup folder path name, e.g., '..\Archive\20070117083021'.
        ''' </summary>
        ''' <param name="instanceName">Specifies the local server instance name, such as 'SQLEXPRESS'</param>
        ''' <param name="pathTitle">Specifies the backup folder path title, e.g., 'Backup'</param>
        ''' <param name="useTimeTampedFolder">Specifies the condition for determining if a time stamped folder is to be
        ''' created for the backup file.</param>
        ''' <returns>The backup file path name.</returns>
        ''' <remarks></remarks>
        Public Shared Function BuildBacupFolderPathName(ByVal instanceName As String, ByVal pathTitle As String, _
            ByVal useTimeTampedFolder As Boolean) As String

            If instanceName Is Nothing Then
                instanceName = String.Empty
            End If

            If String.IsNullOrEmpty(pathTitle) Then
                Throw New ArgumentNullException("pathTitle")
            End If

            Dim lastAction As String

            ' get the path name
            Dim pathName As String = ClientManager.FetchLocalDatabaseDataFolderPathName(instanceName)

            ' get the parent folder of the data folder.
            Dim di As New System.IO.DirectoryInfo(pathName)
            di = di.Parent

            ' create the subfolder requested for backup
            lastAction = "Creating backup folder '{0}'."
            lastAction = String.Format(Globalization.CultureInfo.CurrentCulture, lastAction, di.FullName & "\" & pathTitle)
            My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
            di = di.CreateSubdirectory(pathTitle)

            lastAction = "Setting security access on '{0}'."
            lastAction = String.Format(Globalization.CultureInfo.CurrentCulture, lastAction, di.FullName)
            My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)

            ' Dim directorySecurity As New System.Security.AccessControl.DirectorySecurity
            ' Dim rule As New System.Security.AccessControl.FileSystemAccessRule( _
            '    ClientManager.LocalServerIdentifyName, _
            '    Security.AccessControl.FileSystemRights.Write, Security.AccessControl.AccessControlType.Allow)
            ' directorySecurity.AddAccessRule(rule)
            ' di.SetAccessControl(directorySecurity)

            If useTimeTampedFolder Then

                ' add terminating back slash so as to get the folder corretly identified with FileInfo.
                pathTitle = String.Format(Globalization.CultureInfo.CurrentCulture, "{0:yyyyMMdd}{0:HHmmss}", Now)
                lastAction = "Creating backup folder '{0}'."
                lastAction = String.Format(Globalization.CultureInfo.CurrentCulture, lastAction, di.FullName & "\" & pathTitle)
                My.Application.Log.WriteEntry(lastAction, TraceEventType.Verbose)
                di = di.CreateSubdirectory(pathTitle)
                ' di.SetAccessControl(directorySecurity)

            End If

            pathName = di.FullName
            Return pathName

        End Function

#End If
#End Region

    End Class

End Namespace