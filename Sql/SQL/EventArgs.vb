Namespace Sql

    ''' <summary>
    ''' Provides event arguments for raising data-related events.</summary>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="11/07/06" by="David Hary" revision="1.0.2502.x">
    ''' created.
    ''' </history>
    Public Class EventArgs

        Inherits System.EventArgs

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>
        ''' Constructor with messages and and synopsis information.
        ''' </summary>
        ''' <param name="details">Specifies the message.</param>
        ''' <param name="synopsis">Specifies the synpopsis of the message.</param>
        ''' <param name="isError">True if the message represents an error.</param>
        ''' <remarks></remarks>
        Friend Sub New(ByVal isError As Boolean, ByVal synopsis As String, ByVal details As String)

            ' instantiate the base class
            MyBase.New()

            ' Set the member properties.
            _message = details
            _isError = isError
            _synopsis = synopsis

        End Sub

        ''' <summary>
        ''' Constructor with exception and synopsis information.
        ''' </summary>
        ''' <param name="ex"></param>
        ''' <remarks></remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="For future use")> _
        Friend Sub New(ByVal ex As Exception, ByVal synopsis As String)

            ' instantiate the base class
            Me.New(True, synopsis, ex.Message)

        End Sub

        ''' <summary>
        ''' The cloning constructore.
        ''' </summary>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification:="For future use")> _
        Friend Sub New(ByVal e As Sql.EventArgs)
            Me.New(e.IsError, e.Synopsis, e.Message)
        End Sub

#End Region

#Region " COM PROPERTIES "

        Private _message As String = String.Empty
        ''' <summary>Gets or sets the message of the event arguments.</summary>
        ''' <value><c>Message</c> is a string property.</value>
        Public Property Message() As String
            Get
                Return _message
            End Get
            Set(ByVal Value As String)
                _message = Value
            End Set
        End Property

        Private _isError As Boolean
        ''' <summary>
        ''' Holds true if the bank is reporting an error. 
        ''' </summary>
        Public Property IsError() As Boolean
            Get
                Return _isError
            End Get
            Set(ByVal Value As Boolean)
                _isError = Value
            End Set
        End Property

        Private _synopsis As String = String.Empty
        ''' <summary>Gets or sets the Synopsis of the event arguments.</summary>
        ''' <value><c>Synopsis</c> is a string property.</value>
        Public Property Synopsis() As String
            Get
                Return _synopsis
            End Get
            Set(ByVal Value As String)
                _synopsis = Value
            End Set
        End Property

#End Region

    End Class

End Namespace

