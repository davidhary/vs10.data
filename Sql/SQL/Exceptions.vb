Namespace Sql

    ''' <summary>
    ''' Reports an exception caused because an SQL server was not found.
    ''' </summary>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="11/08/06" by="David Hary" revision="1.0.2503.x">
    ''' created.
    ''' </history>
    <Serializable()> Public Class LocalServerNotFoundException

        Inherits isr.Data.BaseException

        Private Const _defMessage As String = "No local SQL Server is installed."

        ''' <summary>
        ''' <summary>Constructor with no parameters.</summary>
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()
            Me.New(_defMessage)
        End Sub

        ''' <summary>
        ''' Constructs the class specifying a <paramref name="message">message</paramref>.
        ''' </summary>
        ''' <param name="message">
        ''' Specifies the exception message.
        ''' </param>
        Public Sub New(ByVal message As String)
            MyBase.New(message)
        End Sub

        ''' <summary>Constructor specifying the Message and InnerException property to be set.</summary>
        ''' <param name="message">Specifies the exception message.</param>
        ''' <param name="innerException">Sets a reference to the InnerException.</param>
        Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
            MyBase.New(message, innerException)
        End Sub

        ''' <summary>
        ''' Constructs the class specifying a <paramref name="message">message</paramref>
        ''' and <paramref name="innerException"/>.
        ''' </summary>
        ''' <param name="innerException">Sets a reference to the InnerException.</param>
        ''' <param name="format">Specifies the exception message format.</param>
        ''' <param name="args">Specifies the exception message arguments.</param>
        Public Sub New(ByVal innerException As System.Exception, ByVal format As String, ByVal ParamArray args() As Object)
            MyBase.New(innerException, format, args)
        End Sub

        ''' <summary>
        ''' Constructs the class using serialization <paramref name="info"/> and <paramref name="context"/>
        '''  information.
        ''' </summary>
        ''' <param name="info">
        ''' Specifies <see cref="System.Runtime.Serialization.SerializationInfo">serialization information</see>.
        ''' </param>
        ''' <param name="context">
        ''' Sepecifies <see cref="System.Runtime.Serialization.StreamingContext">streaming context</see> for the exception.
        ''' </param>
        Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, _
            ByVal context As System.Runtime.Serialization.StreamingContext)
            MyBase.New(info, context)
        End Sub

    End Class

    ''' <summary>
    ''' Reports an exception caused because an attempt was made to restore a database
    ''' that already exists.
    ''' </summary>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="11/08/06" by="David Hary" revision="1.0.2503.x">
    ''' created.
    ''' </history>
    <Serializable()> Public Class DatabaseAlreadyExistsException

        Inherits isr.Data.BaseException

        Private Const _defMessage As String = "Database '{1}' already exists on the local server '{0}'."

        ''' <summary>Constructor with no parameters.</summary>
        Public Sub New()
            MyBase.New()
            _instanceName = String.Empty
            _catalogName = String.Empty
        End Sub

        ''' <summary>
        ''' Constructs the class specifying a <paramref name="message">message</paramref>.
        ''' </summary>
        ''' <param name="message">
        ''' Specifies the exception message.
        ''' </param>
        Public Sub New(ByVal message As String)
            MyBase.New(message)
        End Sub

        ''' <summary>Constructor specifying the Message and InnerException property to be set.</summary>
        ''' <param name="message">Specifies the exception message.</param>
        ''' <param name="innerException">Sets a reference to the InnerException.</param>
        Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
            Me.New(innerException, message)
        End Sub

        ''' <summary>
        ''' Constructs the class specifying a <paramref name="message">message</paramref>
        ''' and <paramref name="innerException"/>.
        ''' </summary>
        ''' <param name="innerException">Sets a reference to the InnerException.</param>
        ''' <param name="format">Specifies the exception message format.</param>
        ''' <param name="args">Specifies the exception message arguments.</param>
        Public Sub New(ByVal innerException As System.Exception, ByVal format As String, ByVal ParamArray args() As Object)
            MyBase.New(innerException, format, args)
        End Sub

        ''' <summary>Constructor specifying the instance and database names.</summary>
        ''' <param name="localInstanceName">Specifies the name of the local server.</param>
        ''' <param name="databaseName">Specifies the name of the database.</param>
        Public Sub New(ByVal localInstanceName As String, ByVal databaseName As String)
            MyBase.New(String.Format(Globalization.CultureInfo.CurrentCulture, _defMessage, localInstanceName, databaseName))
            _instanceName = localInstanceName
            _catalogName = databaseName
        End Sub

        ''' <summary>
        ''' Constructs the class specifying a <paramref name="message">message</paramref>.
        ''' </summary>
        ''' <param name="message">
        ''' Specifies the exception message.
        ''' </param>
        ''' <param name="localInstanceName">Specifies the name of the local server.</param>
        ''' <param name="databaseName">Specifies the name of the database.</param>
        Public Sub New(ByVal message As String, ByVal localInstanceName As String, ByVal databaseName As String)
            MyBase.New(message)
            _instanceName = localInstanceName
            _catalogName = databaseName
        End Sub

        ''' <summary>
        ''' Constructs the class specifying a <paramref name="message">message</paramref>
        ''' and <paramref name="innerException"/>.
        ''' </summary>
        ''' <param name="message">
        ''' Specifies the exception message.
        ''' </param>
        ''' <param name="localInstanceName">Specifies the name of the local server.</param>
        ''' <param name="databaseName">Specifies the name of the database.</param>
        ''' <param name="innerException">
        ''' Specifies the InnerException.
        ''' </param>
        Public Sub New(ByVal message As String, ByVal localInstanceName As String, ByVal databaseName As String, ByVal innerException As System.Exception)
            Me.New(innerException, message)
            _instanceName = localInstanceName
            _catalogName = databaseName
        End Sub

        ''' <summary>
        ''' Constructs the class using serialization <paramref name="info"/> and <paramref name="context"/>
        '''  information.
        ''' </summary>
        ''' <param name="info">
        ''' Specifies <see cref="System.Runtime.Serialization.SerializationInfo">serialization information</see>.
        ''' </param>
        ''' <param name="context">
        ''' Sepecifies <see cref="System.Runtime.Serialization.StreamingContext">streaming context</see> for the exception.
        ''' </param>
        Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, _
            ByVal context As System.Runtime.Serialization.StreamingContext)
            MyBase.New(info, context)
        End Sub

        Private _instanceName As String
        Public ReadOnly Property InstanceName() As String
            Get
                Return _instanceName
            End Get
        End Property

        Private _catalogName As String
        Public ReadOnly Property CatalogName() As String
            Get
                Return _catalogName
            End Get
        End Property

        ''' <summary>Overrides the GetObjectData method to serialize custom values.</summary>
        ''' <param name="info">Represents the SerializationInfo of the exception.</param>
        ''' <param name="context">Represents the context information of the exception.</param>
        <Security.Permissions.SecurityPermission(Security.Permissions.SecurityAction.Demand, SerializationFormatter:=True)> _
          Public Overrides Sub GetObjectData(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)

            If info Is Nothing Then
                Return
            End If
            info.AddValue("InstanceName", _instanceName, GetType(String))
            info.AddValue("CatalogName", _catalogName, GetType(String))
            MyBase.GetObjectData(info, context)
        End Sub

    End Class

    ''' <summary>
    ''' Reports an exception caused because an attempt was made to access a database
    ''' that was not located.
    ''' </summary>
    ''' <license>
    ''' (c) 2007 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="01/23/07" by="David Hary" revision="1.0.2579.x">
    ''' created.
    ''' </history>
    <Serializable()> Public Class DatabaseNotFoundException

        Inherits isr.Data.BaseException

        Private Const _defMessage As String = "Database '{1}' not found on the local server '{0}'."

        ''' <summary>Constructor with no parameters.</summary>
        Public Sub New()
            MyBase.New()
            _instanceName = String.Empty
            _catalogName = String.Empty
        End Sub

        ''' <summary>
        ''' Constructs the class specifying a <paramref name="message">message</paramref>.
        ''' </summary>
        ''' <param name="message">
        ''' Specifies the exception message.
        ''' </param>
        Public Sub New(ByVal message As String)
            MyBase.New(message)
        End Sub

        ''' <summary>
        ''' Constructs the class specifying a <paramref name="message">message</paramref>
        ''' and <paramref name="innerException"/>.
        ''' </summary>
        ''' <param name="message">
        ''' Specifies the exception message.
        ''' </param>
        ''' <param name="innerException">
        ''' Specifies the InnerException.
        ''' </param>
        Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
            MyBase.New(innerException, message)
        End Sub

        ''' <summary>Constructor specifying the instance and database names.</summary>
        ''' <param name="localInstanceName">Specifies the name of the local server.</param>
        ''' <param name="databaseName">Specifies the name of the database.</param>
        Public Sub New(ByVal localInstanceName As String, ByVal databaseName As String)
            MyBase.New(String.Format(Globalization.CultureInfo.CurrentCulture, _defMessage, localInstanceName, databaseName))
            _instanceName = localInstanceName
            _catalogName = databaseName
        End Sub

        ''' <summary>
        ''' Constructs the class specifying a <paramref name="message">message</paramref>.
        ''' </summary>
        ''' <param name="message">
        ''' Specifies the exception message.
        ''' </param>
        ''' <param name="localInstanceName">Specifies the name of the local server.</param>
        ''' <param name="databaseName">Specifies the name of the database.</param>
        Public Sub New(ByVal message As String, ByVal localInstanceName As String, ByVal databaseName As String)
            MyBase.New(message)
            _instanceName = localInstanceName
            _catalogName = databaseName
        End Sub

        ''' <summary>
        ''' Constructs the class specifying a <paramref name="message">message</paramref>
        ''' and <paramref name="innerException"/>.
        ''' </summary>
        ''' <param name="message">
        ''' Specifies the exception message.
        ''' </param>
        ''' <param name="localInstanceName">Specifies the name of the local server.</param>
        ''' <param name="databaseName">Specifies the name of the database.</param>
        ''' <param name="innerException">
        ''' Specifies the InnerException.
        ''' </param>
        Public Sub New(ByVal message As String, ByVal localInstanceName As String, ByVal databaseName As String, ByVal innerException As System.Exception)
            MyBase.New(message, innerException)
            _instanceName = localInstanceName
            _catalogName = databaseName
        End Sub

        ''' <summary>
        ''' Constructs the class using serialization <paramref name="info"/> and <paramref name="context"/>
        '''  information.
        ''' </summary>
        ''' <param name="info">
        ''' Specifies <see cref="System.Runtime.Serialization.SerializationInfo">serialization information</see>.
        ''' </param>
        ''' <param name="context">
        ''' Sepecifies <see cref="System.Runtime.Serialization.StreamingContext">streaming context</see> for the exception.
        ''' </param>
        Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, _
            ByVal context As System.Runtime.Serialization.StreamingContext)
            MyBase.New(info, context)
        End Sub

        Private _instanceName As String
        Public ReadOnly Property InstanceName() As String
            Get
                Return _instanceName
            End Get
        End Property

        Private _catalogName As String
        Public ReadOnly Property CatalogName() As String
            Get
                Return _catalogName
            End Get
        End Property

        ''' <summary>Overrides the GetObjectData method to serialize custom values.</summary>
        ''' <param name="info">Represents the SerializationInfo of the exception.</param>
        ''' <param name="context">Represents the context information of the exception.</param>
        <Security.Permissions.SecurityPermission(Security.Permissions.SecurityAction.Demand, SerializationFormatter:=True)> _
          Public Overrides Sub GetObjectData(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)

            If info Is Nothing Then
                Return
            End If
            info.AddValue("InstanceName", _instanceName, GetType(String))
            info.AddValue("CatalogName", _catalogName, GetType(String))
            MyBase.GetObjectData(info, context)
        End Sub

    End Class

End Namespace
