﻿Namespace Sql

    ''' <summary>Includes support functionality for SQL server on a remote machine.</summary>
    ''' <license>
    ''' (c) 2008 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="10/23/08" by="David Hary" revision="1.0.3218.x">
    ''' Source: http://www.codeproject.com/useritems/Deploy_your_database.asp.
    ''' Converted to .NET.
    ''' </history>
    Public NotInheritable Class RemoteManager

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>
        ''' Construtor for this class.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()
            Me.New("", "")
        End Sub

        ''' <summary>
        ''' Construtor for this class.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New(ByVal serverName As String, ByVal instanceName As String)
            MyBase.New()
            Me.ServerName = serverName
            Me.InstanceName = instanceName
            Me.DatabaseName = ""
        End Sub

        ''' <summary>
        ''' Construtor for this class.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New(ByVal serverName As String, ByVal instanceName As String, ByVal databaseName As String)
            Me.New(serverName, instanceName)
            Me.DatabaseName = databaseName
        End Sub

        ''' <summary>
        ''' Construtor for this class.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New(ByVal connectionString As String)
            Me.New()
            Me.ParseConnectionString(connectionString)
        End Sub

        ''' <summary>
        ''' Parses the connection string getting the server, instance,and database names.
        ''' </summary>
        ''' <param name="connectionString"></param>
        ''' <remarks></remarks>
        Public Sub ParseConnectionString(ByVal connectionString As String)

            If String.IsNullOrEmpty(connectionString) Then
                Throw New ArgumentNullException("connectionString")
            End If

            ' data source=MUSCAT\SQLEXPRESS;initial catalog=Provers70;persist security info=False;User id=bank;Password=syncWithMe;
            Dim syntaxParts As String() = connectionString.Split(";"c)
            Dim dataSources As String() = syntaxParts(0).Split("="c)
            Dim serverElements As String() = dataSources(1).Split("\"c)
            If serverElements IsNot Nothing Then
                If serverElements.Length > 0 Then
                    Me.ServerName = serverElements(0)
                End If
                If serverElements.Length > 1 Then
                    Me.InstanceName = serverElements(1)
                End If
            End If
            Dim databaselements As String() = syntaxParts(1).Split("="c)
            If databaselements IsNot Nothing Then
                If databaselements.Length > 1 Then
                    Me.DatabaseName = databaselements(1)
                End If
            End If

        End Sub

        Private _databaseName As String
        ''' <summary>
        ''' Gets or sets the database name.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property DatabaseName() As String
            Get
                Return _databaseName
            End Get
            Set(ByVal value As String)
                _databaseName = value
            End Set
        End Property

        Private _instanceName As String = "SQLEXPRESS"
        ''' <summary>
        ''' Gets or sets the instance name, e.g., 'SQLEXPRESS'.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property InstanceName() As String
            Get
                Return _instanceName
            End Get
            Set(ByVal value As String)
                _instanceName = value
            End Set
        End Property

        Private _serverName As String = ""
        ''' <summary>
        ''' Gets or sets the server name, e.g., 'MUSCAT'.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property ServerName() As String
            Get
                Return _serverName
            End Get
            Set(ByVal value As String)
                _serverName = value
            End Set
        End Property

#End Region

#Region " DATABASE "

        Private _isDatabaseExists As Boolean
        ''' <summary>
        ''' Gets true if the database exists.  The database must be located first
        ''' using <see cref="LocateDatabase"/>.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property IsDatabaseExists() As Boolean
            Get
                Return _isDatabaseExists
            End Get
        End Property

        ''' <summary>
        ''' Looks for the the database and returns true if exists.  Sets the local cache 
        ''' <see cref="IsDatabaseExists"/> for the condition.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks>Sets <see cref="IsDatabaseExists"></see></remarks>
        Public Function LocateDatabase(ByVal connectionString As String) As Boolean
            Dim lastAction As String
            lastAction = "Locating database"
            OnMessageAvailable(lastAction, lastAction & " using " & connectionString)
            My.Application.Log.WriteEntry(lastAction & " using " & connectionString, TraceEventType.Verbose)
            _isDatabaseExists = isr.Data.Sql.ClientManager.IsCatalogExists(connectionString)
            Return _isDatabaseExists
        End Function

        ''' <summary>
        ''' Try to connect to the database using the specified connection string.
        ''' </summary>
        ''' <param name="connectionString"></param>
        ''' <returns></returns>
        ''' <remarks>Sets <see cref="IsDatabaseExists"></see></remarks>
        Public Function TryConnect(ByVal connectionString As String) As Boolean
            Dim lastAction As String
            lastAction = "Attempting a connection to the database"
            OnMessageAvailable(lastAction, lastAction & " using " & connectionString)
            My.Application.Log.WriteEntry(lastAction & " using " & connectionString, TraceEventType.Verbose)
            _isDatabaseExists = isr.Data.Sql.ClientManager.TryConnect(connectionString)
            Return _isDatabaseExists
        End Function

#End Region

#Region " EVENTS "

        ''' <summary>The event is raised when the bank as a message to log or display.</summary>
        ''' <param name="e">A reference to the <see cref="T:System.EventArgs"/> event arguments</param>
        ''' <remarks>Must be declared the old way to work with COM.</remarks>
        Public Event MessageAvailable As EventHandler(Of Sql.EventArgs)

        ''' <summary>The event is raised when an error occurred.</summary>
        ''' <param name="e">A reference to the <see cref="T:System.EventArgs"/> event arguments</param>
        ''' <remarks>Must be declared the old way to work with COM.</remarks>
        Public Event ErrorAvailable As EventHandler(Of Sql.EventArgs)

#End Region

#Region " ON EVENT HANDLERS "

        ''' <summary>Logs a message using the COM calling object.  
        ''' Under COM the library cannot log message.</summary>
        Friend Sub OnMessageAvailable(ByVal synopsis As String, ByVal message As String)
            If message Is Nothing Then
                Throw New ArgumentNullException("message")
            End If
            If synopsis Is Nothing Then
                Throw New ArgumentNullException("synopsis")
            End If
            Dim e As New Sql.EventArgs(False, synopsis, message)
            If MessageAvailableEvent IsNot Nothing Then MessageAvailableEvent(Me, e)
        End Sub

#End Region

    End Class

End Namespace
